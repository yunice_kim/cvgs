﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConestogaVirtualGameStore.Framework.ValidationAttributes
{
    public class MinimumAgeValidationAttribute : ValidationAttribute
    {
        int _minimumAge;

        public MinimumAgeValidationAttribute(int minimumAge)
        {
            _minimumAge = minimumAge;
        }
        public override string FormatErrorMessage(string name)
        {
            return "Must be atleast 18 or older.";
        }

        public override bool IsValid(object value)
        {
            DateTime date;
            if (value == null) //for handling null
            {
                return true;
            }
            if (DateTime.TryParse(value.ToString(), out date))
            {
                return date.AddYears(_minimumAge) < DateTime.Now;
            }

            return false;
        }
    }
}
