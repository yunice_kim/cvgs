﻿using ConestogaVirtualGameStore.Framework.ValidationAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConestogaVirtualGameStore.Framework.Entities
{
    public class Person
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PersonId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [MinimumAgeValidation(18)]
        public DateTime? DateOfBirth { get; set; }
        //[Required(ErrorMessage ="Phone Number is required")]
        //[RegularExpression(@"^(+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$",ErrorMessage = "Invalid Phone Number Entered")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Email address")]
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")] 
        public string Email { get; set; }
        public string Gender { get; set; }
        public virtual ICollection<Address> Addresses { get; set; }
        public virtual Address BillingAddress
        {
            get { return Addresses.Where(a => a.AddressType == AddressType.Billing).FirstOrDefault(); }
        }
        public virtual Address ShippingAddress { 
            get { return Addresses.Where(a => a.AddressType == AddressType.Shipping).FirstOrDefault(); } 
        }
        public Person()
        {
            Addresses = new List<Address>();
        }
    }
}
