﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConestogaVirtualGameStore.Framework.Entities
{
    public class Friend
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FriendId { get; set; }
        public int AccountId { get; set; }
        public string userName { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }
        public Friend()
        {

        }
    }
}
