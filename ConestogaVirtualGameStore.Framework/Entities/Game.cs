﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConestogaVirtualGameStore.Framework.Entities
{
    public class Game
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GameId { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Price is required")]
        [Range(0.0, 100.00, ErrorMessage = "Price must be between $0.0 and $100.00")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public double Price { get; set; }
        public int GameCategoryId { get; set; }
        [ForeignKey("GameCategoryId")]
        public virtual GameCategory GameCategory { get; set; }
        public string GamePlatformCode { get; set; }
        [ForeignKey("GamePlatformCode")]
        public virtual GamePlatform GamePlatform { get; set; }
        [DisplayName("Release Date")]
        //[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        //[DataType(DataType.Date)]
        public DateTime? ReleaseDate { get; set; }
        [DisplayName("Supplier")]
        [Required(ErrorMessage = "Supplier is required")]
        public string Supplier { get; set; }
        [Required(ErrorMessage = "Quantity is required")]
        [Range(0, 100, ErrorMessage = "Quantity must be between 0-100")]
        public int Quantity { get; set; }

        public Game()
        {
        }
    }
}
