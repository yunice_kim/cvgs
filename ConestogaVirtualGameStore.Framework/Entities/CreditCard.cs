﻿using ConestogaVirtualGameStore.Framework.ValidationAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConestogaVirtualGameStore.Framework.Entities
{
    public class CreditCard
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CreditCardId { get; set; }
        [Required]
        public string Cardholder { get; set; }
        [Required]
        [CreditCardValidation(AcceptedCardTypes = CreditCardValidationAttribute.CardType.Visa | CreditCardValidationAttribute.CardType.MasterCard)]
        public string CardNumber { get; set; }
        [Required]
        [StringLength(3, ErrorMessage = "CVV must be only 3 digits long.", MinimumLength = 3)]
        [RegularExpression(@"^\d+$", ErrorMessage = "CVV must be numbers/digits.")]
        public string CVV { get; set; }
        [Required]
        public DateTime ExpiryDate { get; set; }
        public int AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }

        public CreditCard()
        {

        }
    }
}
