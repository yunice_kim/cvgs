﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConestogaVirtualGameStore.Framework.Entities
{
    public enum AccountState
    {
        New,
        Active,
        Suspended
    }

    public class Account
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AccountId { get; set; }
        public AccountState AccountState { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        [StringLength(18, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [RegularExpression(@"^.*(?=.{6,18})(?=.*\d)(?=.*[A-Za-z])(?=.*[@%&#]{0,}).*$", ErrorMessage = "Password doesn't meet the requirements")] 
        public string Password { get; set; }
        [Required]
        public bool ReceivePromotionalEmail { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int Attempts { get; set; }
        public string PasswordResetCode { get; set; }
        public int? LastUsedAddressId { get; set; }
        public int? LastUsedCreditCard { get; set; }
        public int PersonId { get; set; }
        [ForeignKey("PersonId")]
        public virtual Person Person { get; set; }
        public virtual ICollection<Preference> Preferences { get; set; }

        public Account()
        {
            Preferences = new List<Preference>();
        }

    }
}
