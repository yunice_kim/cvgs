﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConestogaVirtualGameStore.Framework.Entities
{
    public class GameCategory
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GameCategoryId { get; set; }
        [Required]
        public string CategoryName { get; set; }
        public GameCategory()
        {

        }
    }
}
