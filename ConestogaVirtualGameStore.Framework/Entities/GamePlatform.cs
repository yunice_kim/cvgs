﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConestogaVirtualGameStore.Framework.Entities
{
    public class GamePlatform
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string GamePlatformCode { get; set; }
        [Required]
        public string Name { get; set; }
        public GamePlatform()
        {

        }
    }
}
