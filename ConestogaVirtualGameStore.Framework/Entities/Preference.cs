﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConestogaVirtualGameStore.Framework.Entities
{
    public class Preference
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PreferenceId { get; set; }
        public int GameCategoryId { get; set; }
        [ForeignKey("GameCategoryId")]
        public virtual GameCategory GameCategory { get; set; }
        public string GamePlatformCode { get; set; }
        [ForeignKey("GamePlatformCode")]
        public virtual GamePlatform GamePlatform { get; set; }
        public int AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }

        public Preference()
        {

        }
    }
}
