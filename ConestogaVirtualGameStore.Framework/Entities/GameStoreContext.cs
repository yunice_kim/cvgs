﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Infrastructure;

namespace ConestogaVirtualGameStore.Framework.Entities
{
    public class GameStoreContext : DbContext
    {
        public DbSet<Person> Person { get; set; }
        public DbSet<Address> Address { get; set; }
        public DbSet<Employee> Employee { get; set; }
        public DbSet<Account> Account { get; set; }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<Event> Event { get; set; }
        public DbSet<EventRegister> EventRegister { get; set; }
        public DbSet<Game> Game { get; set; }
        public DbSet<GameCategory> GameCategory { get; set; }
        public DbSet<GamePlatform> GamePlatform { get; set; }
        public DbSet<Cart> Cart { get; set; }
        public DbSet<CreditCard> CreditCard { get; set; }
        public DbSet<LineItem> LineItem { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<Review> Review { get; set; }
        public DbSet<Wishlist> Wishlist { get; set; }
        public DbSet<WishlistItem> WishlistItem { get; set; }
        public DbSet<Friend> Friend { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<UserRole> UserRole { get; set; }

        public GameStoreContext() : base("Name=GameStoreContext")
        {
            var adapter = (IObjectContextAdapter)this;
            var objectContext = adapter.ObjectContext;
            objectContext.CommandTimeout = 30; // value in seconds
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Database.SetInitializer<GameStoreContext>("");
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public System.Data.Entity.DbSet<ConestogaVirtualGameStore.Framework.Entities.Preference> Preferences { get; set; }
    }
}
