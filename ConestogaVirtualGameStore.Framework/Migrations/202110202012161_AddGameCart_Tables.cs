namespace ConestogaVirtualGameStore.Framework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddGameCart_Tables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cart",
                c => new
                    {
                        CartId = c.Int(nullable: false, identity: true),
                        CreatedOn = c.DateTime(nullable: false),
                        AccountId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CartId)
                .ForeignKey("dbo.Account", t => t.AccountId, cascadeDelete: true)
                .Index(t => t.AccountId);
            
            CreateTable(
                "dbo.CreditCard",
                c => new
                    {
                        CreditCardId = c.Int(nullable: false, identity: true),
                        CardNumber = c.String(nullable: false),
                        CVV = c.String(nullable: false),
                        ExpiryDate = c.DateTime(nullable: false),
                        AccountId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CreditCardId)
                .ForeignKey("dbo.Account", t => t.AccountId, cascadeDelete: true)
                .Index(t => t.AccountId);
            
            CreateTable(
                "dbo.LineItem",
                c => new
                    {
                        LineItemId = c.Int(nullable: false, identity: true),
                        Quantity = c.Int(nullable: false),
                        Price = c.Double(nullable: false),
                        GameId = c.Int(nullable: false),
                        CartId = c.Int(nullable: false),
                        OrderId = c.Int(),
                    })
                .PrimaryKey(t => t.LineItemId)
                .ForeignKey("dbo.Cart", t => t.CartId, cascadeDelete: true)
                .ForeignKey("dbo.Game", t => t.GameId, cascadeDelete: true)
                .ForeignKey("dbo.Order", t => t.OrderId)
                .Index(t => t.GameId)
                .Index(t => t.CartId)
                .Index(t => t.OrderId);
            
            CreateTable(
                "dbo.Order",
                c => new
                    {
                        OrderId = c.Int(nullable: false, identity: true),
                        OrderStatus = c.Int(nullable: false),
                        ProcessedDate = c.DateTime(),
                        OrderDate = c.DateTime(nullable: false),
                        OrderTotal = c.Double(nullable: false),
                        AccountId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderId)
                .ForeignKey("dbo.Account", t => t.AccountId, cascadeDelete: true)
                .Index(t => t.AccountId);
            
            CreateTable(
                "dbo.Review",
                c => new
                    {
                        ReviewId = c.Int(nullable: false, identity: true),
                        Ratings = c.Int(nullable: false),
                        Details = c.String(),
                        GameId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ReviewId)
                .ForeignKey("dbo.Game", t => t.GameId, cascadeDelete: true)
                .Index(t => t.GameId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Review", "GameId", "dbo.Game");
            DropForeignKey("dbo.LineItem", "OrderId", "dbo.Order");
            DropForeignKey("dbo.Order", "AccountId", "dbo.Account");
            DropForeignKey("dbo.LineItem", "GameId", "dbo.Game");
            DropForeignKey("dbo.LineItem", "CartId", "dbo.Cart");
            DropForeignKey("dbo.CreditCard", "AccountId", "dbo.Account");
            DropForeignKey("dbo.Cart", "AccountId", "dbo.Account");
            DropIndex("dbo.Review", new[] { "GameId" });
            DropIndex("dbo.Order", new[] { "AccountId" });
            DropIndex("dbo.LineItem", new[] { "OrderId" });
            DropIndex("dbo.LineItem", new[] { "CartId" });
            DropIndex("dbo.LineItem", new[] { "GameId" });
            DropIndex("dbo.CreditCard", new[] { "AccountId" });
            DropIndex("dbo.Cart", new[] { "AccountId" });
            DropTable("dbo.Review");
            DropTable("dbo.Order");
            DropTable("dbo.LineItem");
            DropTable("dbo.CreditCard");
            DropTable("dbo.Cart");
        }
    }
}
