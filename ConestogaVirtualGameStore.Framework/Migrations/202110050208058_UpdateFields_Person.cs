namespace ConestogaVirtualGameStore.Framework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateFields_Person : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Person", "DateOfBirth", c => c.DateTime());
            AlterColumn("dbo.Person", "PhoneNumber", c => c.String());
            AlterColumn("dbo.Person", "Gender", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Person", "Gender", c => c.String(nullable: false));
            AlterColumn("dbo.Person", "PhoneNumber", c => c.String(nullable: false));
            AlterColumn("dbo.Person", "DateOfBirth", c => c.DateTime(nullable: false));
        }
    }
}
