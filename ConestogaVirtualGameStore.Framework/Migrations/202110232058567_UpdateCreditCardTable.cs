namespace ConestogaVirtualGameStore.Framework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateCreditCardTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CreditCard", "Cardholder", c => c.String(nullable: false));
            AlterColumn("dbo.CreditCard", "CVV", c => c.String(nullable: false, maxLength: 3));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CreditCard", "CVV", c => c.String(nullable: false));
            DropColumn("dbo.CreditCard", "Cardholder");
        }
    }
}
