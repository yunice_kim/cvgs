namespace ConestogaVirtualGameStore.Framework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddApproved_RequiredDetails_ReviewTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Review", "Approved", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Review", "Details", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Review", "Details", c => c.String());
            DropColumn("dbo.Review", "Approved");
        }
    }
}
