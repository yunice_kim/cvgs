namespace ConestogaVirtualGameStore.Framework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFKAddressId_OrderTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Order", "AddressId", c => c.Int(nullable: false));
            CreateIndex("dbo.Order", "AddressId");
            AddForeignKey("dbo.Order", "AddressId", "dbo.Address", "AddressId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Order", "AddressId", "dbo.Address");
            DropIndex("dbo.Order", new[] { "AddressId" });
            DropColumn("dbo.Order", "AddressId");
        }
    }
}
