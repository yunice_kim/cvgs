namespace ConestogaVirtualGameStore.Framework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_EventRegister_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EventRegister",
                c => new
                    {
                        EventRegisterId = c.Int(nullable: false, identity: true),
                        EventId = c.Int(nullable: false),
                        AccountId = c.Int(nullable: false),
                        DateRegistered = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.EventRegisterId)
                .ForeignKey("dbo.Account", t => t.AccountId, cascadeDelete: true)
                .ForeignKey("dbo.Event", t => t.EventId, cascadeDelete: true)
                .Index(t => t.EventId)
                .Index(t => t.AccountId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EventRegister", "EventId", "dbo.Event");
            DropForeignKey("dbo.EventRegister", "AccountId", "dbo.Account");
            DropIndex("dbo.EventRegister", new[] { "AccountId" });
            DropIndex("dbo.EventRegister", new[] { "EventId" });
            DropTable("dbo.EventRegister");
        }
    }
}
