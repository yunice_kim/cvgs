namespace ConestogaVirtualGameStore.Framework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddForeignKeys_GameAndPreferenceTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Preference", "GameCategoryId", c => c.Int(nullable: false));
            AddColumn("dbo.Preference", "GamePlatformCode", c => c.String(maxLength: 128));
            AddColumn("dbo.Game", "GameCategoryId", c => c.Int(nullable: false));
            AddColumn("dbo.Game", "GamePlatformCode", c => c.String(maxLength: 128));
            CreateIndex("dbo.Preference", "GameCategoryId");
            CreateIndex("dbo.Preference", "GamePlatformCode");
            CreateIndex("dbo.Game", "GameCategoryId");
            CreateIndex("dbo.Game", "GamePlatformCode");
            AddForeignKey("dbo.Preference", "GameCategoryId", "dbo.GameCategory", "GameCategoryId", cascadeDelete: true);
            AddForeignKey("dbo.Preference", "GamePlatformCode", "dbo.GamePlatform", "GamePlatformCode");
            AddForeignKey("dbo.Game", "GameCategoryId", "dbo.GameCategory", "GameCategoryId", cascadeDelete: true);
            AddForeignKey("dbo.Game", "GamePlatformCode", "dbo.GamePlatform", "GamePlatformCode");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Game", "GamePlatformCode", "dbo.GamePlatform");
            DropForeignKey("dbo.Game", "GameCategoryId", "dbo.GameCategory");
            DropForeignKey("dbo.Preference", "GamePlatformCode", "dbo.GamePlatform");
            DropForeignKey("dbo.Preference", "GameCategoryId", "dbo.GameCategory");
            DropIndex("dbo.Game", new[] { "GamePlatformCode" });
            DropIndex("dbo.Game", new[] { "GameCategoryId" });
            DropIndex("dbo.Preference", new[] { "GamePlatformCode" });
            DropIndex("dbo.Preference", new[] { "GameCategoryId" });
            DropColumn("dbo.Game", "GamePlatformCode");
            DropColumn("dbo.Game", "GameCategoryId");
            DropColumn("dbo.Preference", "GamePlatformCode");
            DropColumn("dbo.Preference", "GameCategoryId");
        }
    }
}
