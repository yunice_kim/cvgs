namespace ConestogaVirtualGameStore.Framework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmptyMigration : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.Role",
            //    c => new
            //        {
            //            RoleId = c.Int(nullable: false, identity: true),
            //            RoleName = c.String(nullable: false),
            //        })
            //    .PrimaryKey(t => t.RoleId);
            
            //CreateTable(
            //    "dbo.UserRole",
            //    c => new
            //        {
            //            UserRoleId = c.Int(nullable: false, identity: true),
            //            RoleId = c.Int(nullable: false),
            //            AccountId = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.UserRoleId)
            //    .ForeignKey("dbo.Account", t => t.AccountId, cascadeDelete: true)
            //    .ForeignKey("dbo.Role", t => t.RoleId, cascadeDelete: true)
            //    .Index(t => t.RoleId)
            //    .Index(t => t.AccountId);
            
            //AddColumn("dbo.Review", "Approved", c => c.Boolean(nullable: false));
            //AlterColumn("dbo.Review", "Details", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            //DropForeignKey("dbo.UserRole", "RoleId", "dbo.Role");
            //DropForeignKey("dbo.UserRole", "AccountId", "dbo.Account");
            //DropIndex("dbo.UserRole", new[] { "AccountId" });
            //DropIndex("dbo.UserRole", new[] { "RoleId" });
            //AlterColumn("dbo.Review", "Details", c => c.String());
            //DropColumn("dbo.Review", "Approved");
            //DropTable("dbo.UserRole");
            //DropTable("dbo.Role");
        }
    }
}
