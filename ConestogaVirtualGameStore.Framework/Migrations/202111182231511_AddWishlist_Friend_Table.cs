namespace ConestogaVirtualGameStore.Framework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddWishlist_Friend_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Friend",
                c => new
                    {
                        FriendId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        userName = c.String(),
                    })
                .PrimaryKey(t => t.FriendId)
                .ForeignKey("dbo.Account", t => t.AccountId, cascadeDelete: true)
                .Index(t => t.AccountId);
            
            CreateTable(
                "dbo.Wishlist",
                c => new
                    {
                        WishlistId = c.Int(nullable: false, identity: true),
                        CreatedDate = c.DateTime(nullable: false),
                        AccountId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.WishlistId)
                .ForeignKey("dbo.Account", t => t.AccountId, cascadeDelete: true)
                .Index(t => t.AccountId);
            
            CreateTable(
                "dbo.WishlistItem",
                c => new
                    {
                        WishlistItemId = c.Int(nullable: false, identity: true),
                        WishlistId = c.Int(nullable: false),
                        GameId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.WishlistItemId)
                .ForeignKey("dbo.Game", t => t.GameId, cascadeDelete: true)
                .ForeignKey("dbo.Wishlist", t => t.WishlistId, cascadeDelete: true)
                .Index(t => t.WishlistId)
                .Index(t => t.GameId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WishlistItem", "WishlistId", "dbo.Wishlist");
            DropForeignKey("dbo.WishlistItem", "GameId", "dbo.Game");
            DropForeignKey("dbo.Wishlist", "AccountId", "dbo.Account");
            DropForeignKey("dbo.Friend", "AccountId", "dbo.Account");
            DropIndex("dbo.WishlistItem", new[] { "GameId" });
            DropIndex("dbo.WishlistItem", new[] { "WishlistId" });
            DropIndex("dbo.Wishlist", new[] { "AccountId" });
            DropIndex("dbo.Friend", new[] { "AccountId" });
            DropTable("dbo.WishlistItem");
            DropTable("dbo.Wishlist");
            DropTable("dbo.Friend");
        }
    }
}
