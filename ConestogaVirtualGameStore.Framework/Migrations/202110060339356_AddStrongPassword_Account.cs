namespace ConestogaVirtualGameStore.Framework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStrongPassword_Account : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Account", "Password", c => c.String(nullable: false, maxLength: 18));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Account", "Password", c => c.String(nullable: false));
        }
    }
}
