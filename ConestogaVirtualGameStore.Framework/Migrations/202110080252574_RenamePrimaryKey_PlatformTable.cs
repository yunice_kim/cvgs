namespace ConestogaVirtualGameStore.Framework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenamePrimaryKey_PlatformTable : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.GamePlatform");
            AddColumn("dbo.GamePlatform", "GamePlatformCode", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.GamePlatform", "GamePlatformCode");
            DropColumn("dbo.GamePlatform", "Code");
        }
        
        public override void Down()
        {
            AddColumn("dbo.GamePlatform", "Code", c => c.String(nullable: false, maxLength: 128));
            DropPrimaryKey("dbo.GamePlatform");
            DropColumn("dbo.GamePlatform", "GamePlatformCode");
            AddPrimaryKey("dbo.GamePlatform", "Code");
        }
    }
}
