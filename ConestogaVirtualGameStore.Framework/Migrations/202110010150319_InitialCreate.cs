﻿namespace ConestogaVirtualGameStore.Framework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Account",
                c => new
                    {
                        AccountId = c.Int(nullable: false, identity: true),
                        AccountState = c.Int(nullable: false),
                        UserName = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        ReceivePromotionalEmail = c.Boolean(nullable: false, defaultValue: false),
                        CreatedOn = c.DateTime(),
                        PersonId = c.Int(nullable: false),
                        Attempts = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.AccountId)
                .ForeignKey("dbo.Person", t => t.PersonId, cascadeDelete: true)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.Person",
                c => new
                    {
                        PersonId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        DateOfBirth = c.DateTime(nullable: false),
                        PhoneNumber = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Gender = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.PersonId);
            
            CreateTable(
                "dbo.Address",
                c => new
                    {
                        AddressId = c.Int(nullable: false, identity: true),
                        Street = c.String(),
                        City = c.String(),
                        Province = c.String(),
                        Country = c.String(),
                        PostalCode = c.String(),
                        PersonId = c.Int(nullable: false),
                        AddressType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AddressId)
                .ForeignKey("dbo.Person", t => t.PersonId, cascadeDelete: true)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.Preference",
                c => new
                    {
                        PreferenceId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PreferenceId)
                .ForeignKey("dbo.Account", t => t.AccountId, cascadeDelete: true)
                .Index(t => t.AccountId);

            CreateTable(
                "dbo.GameCategory",
                c => new
                {
                    GameCategoryId = c.Int(nullable: false, identity: true),
                    CategoryName = c.String(nullable: false)
                })
                .PrimaryKey(t => t.GameCategoryId);

            CreateTable(
                "dbo.GamePlatform",
                c => new
                {
                    Code = c.String(nullable: false, maxLength: 128),
                    Name = c.String(nullable: false)
                })
                .PrimaryKey(t => t.Code);
            
            CreateTable(
                "dbo.Customer",
                c => new
                    {
                        CustomerId = c.Int(nullable: false, identity: true),
                        PersonId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CustomerId)
                .ForeignKey("dbo.Person", t => t.PersonId, cascadeDelete: true)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.Employee",
                c => new
                    {
                        EmployeeId = c.Int(nullable: false, identity: true),
                        Position = c.String(nullable: false),
                        Department = c.String(nullable: false),
                        HireDate = c.DateTime(),
                        PersonId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EmployeeId)
                .ForeignKey("dbo.Person", t => t.PersonId, cascadeDelete: true)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.Event",
                c => new
                    {
                        EventId = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.EventId);
            
            CreateTable(
                "dbo.Game",
                c => new
                    {
                        GameId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        Price = c.Double(nullable: false, defaultValue: 0),
                        ReleaseDate = c.DateTime(),
                        Supplier = c.String(nullable: false),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.GameId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Employee", "PersonId", "dbo.Person");
            DropForeignKey("dbo.Customer", "PersonId", "dbo.Person");
            DropForeignKey("dbo.Preference", "AccountId", "dbo.Account");
            DropForeignKey("dbo.Account", "PersonId", "dbo.Person");
            DropForeignKey("dbo.Address", "PersonId", "dbo.Person");
            DropIndex("dbo.Employee", new[] { "PersonId" });
            DropIndex("dbo.Customer", new[] { "PersonId" });
            DropIndex("dbo.Preference", new[] { "AccountId" });
            DropIndex("dbo.Address", new[] { "PersonId" });
            DropIndex("dbo.Account", new[] { "PersonId" });
            DropTable("dbo.Game");
            DropTable("dbo.Event");
            DropTable("dbo.Employee");
            DropTable("dbo.Customer");
            DropTable("dbo.GamePlatform");
            DropTable("dbo.GameCategory");
            DropTable("dbo.Preference");
            DropTable("dbo.Address");
            DropTable("dbo.Person");
            DropTable("dbo.Account");
        }
    }
}
