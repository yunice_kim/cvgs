namespace ConestogaVirtualGameStore.Framework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOption_LineItem_Table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LineItem", "Option", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.LineItem", "Option");
        }
    }
}
