namespace ConestogaVirtualGameStore.Framework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFKCreditCardId_OrderTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Order", "CreditCardId", c => c.Int(nullable: false));
            CreateIndex("dbo.Order", "CreditCardId");
            AddForeignKey("dbo.Order", "CreditCardId", "dbo.CreditCard", "CreditCardId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Order", "CreditCardId", "dbo.CreditCard");
            DropIndex("dbo.Order", new[] { "CreditCardId" });
            DropColumn("dbo.Order", "CreditCardId");
        }
    }
}
