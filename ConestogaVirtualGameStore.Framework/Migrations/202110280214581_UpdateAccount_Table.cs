namespace ConestogaVirtualGameStore.Framework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateAccount_Table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Account", "LastUsedAddressId", c => c.Int(nullable: true));
            AddColumn("dbo.Account", "LastUsedCreditCard", c => c.Int(nullable: true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Account", "LastUsedCreditCard");
            DropColumn("dbo.Account", "LastUsedAddressId");
        }
    }
}
