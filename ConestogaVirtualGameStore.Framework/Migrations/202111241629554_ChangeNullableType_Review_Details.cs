namespace ConestogaVirtualGameStore.Framework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeNullableType_Review_Details : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Review", "Details", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Review", "Details", c => c.String(nullable: false));
        }
    }
}
