namespace ConestogaVirtualGameStore.Framework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_PasswordResetColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Account", "PasswordResetCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Account", "PasswordResetCode");
        }
    }
}
