﻿namespace ConestogaVirtualGameStore.Framework.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using ConestogaVirtualGameStore.Framework.Entities; 
    internal sealed class Configuration : DbMigrationsConfiguration<Entities.GameStoreContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(GameStoreContext context)
        {
            context.GameCategory.AddOrUpdate(x => x.GameCategoryId,
                new GameCategory()
                {
                    GameCategoryId = 1,
                    CategoryName = "Action"
                },
                new GameCategory()
                {
                    GameCategoryId = 2,
                    CategoryName = "Adventure"
                },
                new GameCategory()
                {
                    GameCategoryId = 3,
                    CategoryName = "Puzzle"
                },
                new GameCategory()
                {
                    GameCategoryId = 4,
                    CategoryName = "Sports"
                },
                new GameCategory()
                {
                    GameCategoryId = 5,
                    CategoryName = "Strategy"
                },
                new GameCategory()
                {
                    GameCategoryId = 6,
                    CategoryName = "Racing"
                }
            );


            context.GamePlatform.AddOrUpdate(x => x.GamePlatformCode,
                new GamePlatform()
                {
                    GamePlatformCode = "DS",
                    Name = "Nintentdo DS"
                },
                new GamePlatform()
                {
                    GamePlatformCode = "PC",
                    Name = "Personal Computer Games"
                },
                new GamePlatform()
                {
                    GamePlatformCode = "PS",
                    Name = "Sony PlayStation"
                }, 
                new GamePlatform()
                {
                    GamePlatformCode = "XBox",
                    Name = "Microsoft XBox 360"
                }
            );

            context.Game.AddOrUpdate(x => x.GameId,
                new Game()
                {
                    GameId = 1,
                    Name = "Overcooked",
                    Description = "Overcooked",
                    Price = 30,
                    ReleaseDate = DateTime.Now,
                    Supplier = "Nintendo",
                    Quantity = 5,
                    GameCategoryId = 1,
                    GamePlatformCode = "DS"
                },
                new Game()
                {
                    GameId = 2,
                    Name = "Minecraft",
                    Description = "Minecraft",
                    Price = 50,
                    ReleaseDate = DateTime.Now,
                    Supplier = "Microsoft",
                    Quantity = 8,
                    GameCategoryId = 2,
                    GamePlatformCode = "XBox"
                },
                new Game()
                {
                    GameId = 3,
                    Name = "CookieRun",
                    Description = "CookieRun",
                    Price = 10,
                    ReleaseDate = DateTime.Now,
                    Supplier = "Idont know",
                    Quantity = 7,
                    GameCategoryId = 6,
                    GamePlatformCode = "PC"
                }               
            );

            context.Person.AddOrUpdate(x => x.PersonId,
                new Person()
                {
                    PersonId = 1,
                    FirstName = "first",
                    LastName = "user",
                    Email = "test1@gmail.com"
                },
                new Person()
                {
                    PersonId = 2,
                    FirstName = "second",
                    LastName = "user",
                    Email = "test2@gmail.com"
                },
                new Person()
                {
                    PersonId = 3,
                    FirstName = "third",
                    LastName = "user",
                    Email = "test3@gmail.com"
                },
                new Person()
                {
                    PersonId = 4,
                    FirstName = "first",
                    LastName = "employee",
                    Email = "testemp1@gmail.com"
                },
                new Person()
                {
                    PersonId = 5,
                    FirstName = "second",
                    LastName = "employee",
                    Email = "testemp2@gmail.com"
                },
                new Person()
                {
                    PersonId = 6,
                    FirstName = "third",
                    LastName = "employee",
                    Email = "testemp3@gmail.com"
                }
            );

            context.Customer.AddOrUpdate(x => x.CustomerId,
                new Customer()
                {
                    CustomerId = 1,
                    PersonId = 1
                },
                new Customer()
                {
                    CustomerId = 2,
                    PersonId = 2
                },
                new Customer()
                {
                    CustomerId = 3,
                    PersonId = 3
                }
            );

            context.Employee.AddOrUpdate(x => x.EmployeeId,
                new Employee()
                {
                    EmployeeId = 1,
                    Position = "Associate",
                    Department = "Development",
                    PersonId = 4
                },
                new Employee()
                {
                    EmployeeId = 2,
                    Position = "Senior Developer",
                    Department = "Development",
                    PersonId = 5
                },
                new Employee()
                {
                    EmployeeId = 3,
                    Position = "Manager",
                    Department = "Development",
                    PersonId = 6
                }
            );

            context.Address.AddOrUpdate(x => x.AddressId,
                new Address()
                {
                    AddressId = 1,
                    Street = "111 Kingsway Street",
                    City = "Kitchener",
                    Province = "ON",
                    Country = "Canada",
                    PostalCode = "A1A 1A1",
                    PersonId = 1,
                    AddressType = 0
                },
                new Address()
                {
                    AddressId = 2,
                    PersonId = 1,
                    AddressType = AddressType.Shipping
                },
                new Address()
                {
                    AddressId = 3,
                    PersonId = 2,
                    Street = "20 Strange Street",
                    City = "Waterloo",
                    Province = "ON",
                    Country = "Canada",
                    PostalCode = "B1B 1B1",
                    AddressType = 0
                },
                new Address()
                {
                    AddressId = 4,
                    PersonId = 2,
                    AddressType = AddressType.Shipping
                },
                new Address()
                {
                    AddressId = 5,
                    PersonId = 3,
                    AddressType = 0
                },
                new Address()
                {
                    AddressId = 6,
                    PersonId = 3,
                    AddressType = AddressType.Shipping
                },
                new Address()
                {
                    AddressId = 7,
                    PersonId = 4,
                    AddressType = 0
                },
                new Address()
                {
                    AddressId = 8,
                    PersonId = 4,
                    AddressType = AddressType.Shipping
                },
                new Address()
                {
                    AddressId = 9,
                    PersonId = 5,
                    AddressType = 0
                },
                new Address()
                {
                    AddressId = 10,
                    PersonId = 5,
                    AddressType = AddressType.Shipping
                },
                new Address()
                {
                    AddressId = 11,
                    PersonId = 6,
                    AddressType = 0
                },
                new Address()
                {
                    AddressId = 12,
                    PersonId = 6,
                    AddressType = AddressType.Shipping
                }
            );

            context.Account.AddOrUpdate(x => x.AccountId,
                new Account()
                {
                    AccountId = 1,
                    AccountState = AccountState.Active,
                    UserName = "test1",
                    Password = "test111",
                    ReceivePromotionalEmail = false,
                    PersonId = 1,
                    Attempts = 0
                },
                new Account()
                {
                    AccountId = 2,
                    AccountState = AccountState.Active,
                    UserName = "test2",
                    Password = "test222",
                    ReceivePromotionalEmail = false,
                    PersonId = 2,
                    Attempts = 0
                },
                new Account()
                {
                    AccountId = 3,
                    AccountState = AccountState.Active,
                    UserName = "test3",
                    Password = "test333",
                    ReceivePromotionalEmail = false,
                    PersonId = 3,
                    Attempts = 0
                },
                new Account()
                {
                    AccountId = 4,
                    AccountState = AccountState.Active,
                    UserName = "testemp1",
                    Password = "testemp111",
                    ReceivePromotionalEmail = false,
                    PersonId = 4,
                    Attempts = 0
                },
                new Account()
                {
                    AccountId = 5,
                    AccountState = AccountState.Active,
                    UserName = "testemp2",
                    Password = "testemp222",
                    ReceivePromotionalEmail = false,
                    PersonId = 5,
                    Attempts = 0
                },
                new Account()
                {
                    AccountId = 6,
                    AccountState = AccountState.Active,
                    UserName = "testemp3",
                    Password = "testemp333",
                    ReceivePromotionalEmail = false,
                    PersonId = 6,
                    Attempts = 0
                }
            );
            context.Role.AddOrUpdate(x => x.RoleId,
                new Role()
                {
                    RoleId = 1,
                    RoleName = "Customer"
                },
                new Role()
                {
                    RoleId = 2,
                    RoleName = "Employee"
                }
            );
            context.UserRole.AddOrUpdate(x => x.UserRoleId,
                new UserRole()
                {
                    UserRoleId = 1,
                    RoleId = 1,
                    AccountId = 1
                },
                new UserRole()
                {
                    UserRoleId = 2,
                    RoleId = 1,
                    AccountId = 2
                },
                new UserRole()
                {
                    UserRoleId = 3,
                    RoleId = 1,
                    AccountId = 3
                },
                new UserRole()
                {
                    UserRoleId = 4,
                    RoleId = 2,
                    AccountId = 4
                },
                new UserRole()
                {
                    UserRoleId = 5,
                    RoleId = 2,
                    AccountId = 5
                },
                new UserRole()
                {
                    UserRoleId = 6,
                    RoleId = 2,
                    AccountId = 6
                }
            );

            //for the test1 user,
            //add 2 items in the cart
            context.Cart.AddOrUpdate(x => x.CartId,
                new Cart()
                {
                    CartId = 1,
                    CreatedOn = DateTime.Now,
                    AccountId = 1
                },
                new Cart()
                {
                    CartId = 2,
                    CreatedOn = DateTime.Now,
                    AccountId = 2
                }
            );

            context.LineItem.AddOrUpdate(x => x.LineItemId,
                new LineItem()
                {
                    LineItemId = 1,
                    Quantity = 1,
                    Price = 30,
                    GameId = 1,
                    CartId = 1,
                    Option="Digital",
                    OrderId = null
                },
                new LineItem()
                {
                    LineItemId = 2,
                    Quantity = 1,
                    Price = 10,
                    GameId = 2,
                    CartId = 1,
                    Option = "Physical",
                    OrderId = null
                },
                //for the Order Details test,
                //Add an lineitem for Order for the test2 user
                new LineItem()
                {
                    LineItemId = 3,
                    Quantity = 1,
                    Price = 10,
                    GameId = 1,
                    CartId = 2,
                    Option = "Physical",
                    OrderId = 1
                }
            );

            //for the test1 user,
            //add the 1st credit card
            context.CreditCard.AddOrUpdate(x => x.CreditCardId,
                new CreditCard()
                {
                    CreditCardId = 1,
                    CardNumber = "5555555555554444",
                    CVV = "123",
                    ExpiryDate = DateTime.Now.AddYears(2),
                    AccountId = 1,
                    Cardholder = "TEST1 MASTER"
                },
                new CreditCard()
                {
                    CreditCardId = 2,
                    CardNumber = "4111111111111111",
                    CVV = "222",
                    ExpiryDate = DateTime.Now.AddYears(2),
                    AccountId = 2,
                    Cardholder = "TEST2 VISA"
                }

            );

            context.Order.AddOrUpdate(x => x.OrderId,
                new Order()
                {   
                    OrderId = 1,
                    OrderStatus = 0,
                    OrderDate = DateTime.Now,
                    OrderTotal = 30,
                    AccountId = 2,
                    CreditCardId = 2,
                    AddressId = 3
                }
            );

            context.Review.AddOrUpdate(x => x.ReviewId,
                new Review()
                {
                    ReviewId = 1,
                    Ratings = 0,
                    Details = "test review for testing",
                    GameId = 1,
                    Approved = false
                }
            );
        }
    }
}
