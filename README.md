## About CVGS(Conestoga Virtual Game Store)
CVGS is a website that maintains membership and provides features that engage members. This application shows the list of various games available in the game store that allows for a systematic organization of the games, inventory, purchases, order history, and users. This application serves two sides, the customer and management as well. Customers can create an account, view games, review, rate, purchase games, form a Wishlist, share with other members on social media, maintain track of an order, and display order status. The inventory status is updated when the order is placed with the current quantity of games present in stock. Whenever an item is requested for purchase, it checks for availability, and if there is no availability, the user gets notified. An employee(management) can also create an employee account, approve game reviews, add, delete, update records of games, manage inventory and orders.

## How to install the application
1. Clone the source code on the local.
2. Open the file 'ConestogaVirtualGameStore.Web.sln' under ConestogaVirtualGameStore.Web folder on a Visual Studio 2019 or latest version.
3. Click the 'Run' button with IIS Express, then the Web Application is running locally

## How to create the Database(MS SQL Server)
1. Check if you are connected to the SQL server.
2. If not  select server type like  Database engine, Server name  click on connect.
3. The server’s name appears on the left side of the screen on the object explorer.
4. Right-click Databases  select New Database.
5. In New database  enter the database name, the owner (or select default settings)  click on OK.
6. Refresh the Databases, on the databases  click + to expand it  you will see the database with the chosen database name

## License
Click here: https://www.eulatemplate.com/live.php?token=bw3EWCPkMYxEjNgY4GCfaKSgYgER2JWc<br/>
The reason I chose this license is simple and easy to create.

