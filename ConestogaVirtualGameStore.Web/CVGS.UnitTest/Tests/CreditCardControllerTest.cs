﻿using ConestogaVirtualGameStore.Framework.Entities;
using ConestogaVirtualGameStore.Web.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CVGS.UnitTest.Tests
{
    [TestClass]
    public class CreditCardControllerTest
    {
        [TestMethod]
        public void TestCreateNewCreditCard()
        {
            // Arrange
            CreditCardController controller = new CreditCardController();
            CreditCard creditCard = new CreditCard();

            //creditCard.CreditCardId = 3;
            creditCard.Cardholder = "JOHN WILLIAMS";
            creditCard.CardNumber = "4012888888881881";
            creditCard.CVV = "523";
            creditCard.ExpiryDate = Convert.ToDateTime("2023-01-01 12:00:00.000");
            creditCard.AccountId = 1;

            // Act
            RedirectToRouteResult result = (RedirectToRouteResult)controller.Create(creditCard, creditCard.AccountId);

            // Assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
        }

        [TestMethod]
        public void TestCreateDuplicateCreditCard()
        {
            // Arrange
            CreditCardController controller = new CreditCardController();
            CreditCard creditCard = new CreditCard();

            //creditCard.CreditCardId = 6;
            creditCard.Cardholder = "RYAN JONAS";
            creditCard.CardNumber = "4012888888881881";
            creditCard.CVV = "435";
            creditCard.ExpiryDate = Convert.ToDateTime("2023-01-01 12:00:00.000");
            creditCard.AccountId = 1;

            // Act
            ViewResult result = (ViewResult)controller.Create(creditCard, creditCard.AccountId);

            // Assert
            Assert.AreEqual("Create", result.ViewName);
        }
    }
}
