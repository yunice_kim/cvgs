﻿using ConestogaVirtualGameStore.Framework.Entities;
using ConestogaVirtualGameStore.Web.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CVGS.UnitTest.Tests
{
    [TestClass]
    class WishlistControllerTest
    {
        WishlistController controller = new WishlistController();
        private GameStoreContext db = new GameStoreContext();
        [TestMethod]
        public void TestAddToWishlist()
        {
            //Arrange
            int gameId=1;
            int accountId = 1;
            string keyword = "ver";
            //string userName = "test2";

            //Act
            ViewResultBase result = (ViewResultBase)controller.AddToWishlist(gameId, accountId, keyword);

            //Assert
            Assert.AreEqual("SearchGameWithKeyword", result.ViewName);

        }
    }
}
