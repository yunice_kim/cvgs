﻿using ConestogaVirtualGameStore.Framework.Entities;
using ConestogaVirtualGameStore.Web.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;

namespace CVGS.UnitTest.Tests
{
 

    [TestClass]
    public class CartControllerTest
    {
        

        [TestMethod]
        public void TestDetails()
        {
            //Arrange
            var controller = new CartController();
            int itemId = 1;
            int accountId = 1;

            //Act
            ViewResultBase result = (ViewResultBase)controller.Details(itemId, accountId);

            //Assert
            Assert.AreEqual("", result.ViewName);
        }

        [TestMethod]
        public void TestAddItem()
        {
            //Arrange
            var controller = new CartController();
            Game gameItem = new Game
            {
                GameId = 1,
                Quantity = 1
            };
            string option = "Physical";
            //int itemId = 1;
            int accountId = 1;

            //Act
            RedirectToRouteResult result = (RedirectToRouteResult)controller.AddItem(gameItem,option, accountId);

            //Assert
            Assert.AreEqual("", result.RouteName);
        }

        [TestMethod]
        public void TestEditGameQuantity()
        {
            //Arrange
            var controller = new CartController();
            Game gameItem = new Game
            {
                GameId = 1,
                Quantity = 1
            };

            int lineItemId = 1;
            //int accountId = 1;

            //Act
            RedirectToRouteResult result = (RedirectToRouteResult)controller.Edit(gameItem, lineItemId);

            //Assert
            Assert.AreEqual("", result.RouteName);
        }

        [TestMethod]
        public void TestCheckout()
        {
            //Arrange
            var controller = new CartController();

            int cartId = 1;
            int accountId = 1;

            //Act
            ViewResultBase result = (ViewResultBase)controller.Checkout(accountId, cartId);

            //Assert
            Assert.AreEqual("CheckoutDetails", result.ViewName);
        }

        //[TestMethod]
        //public void TestProceed()
        //{
        //need to use accoundId from session
        //    //Arrange
        //    var controller = new CartController();

        //    int cartId = 1;
        //    int accountId = 1;

        //    //Act
        //    ViewResultBase result = (ViewResultBase)controller.Checkout(accountId, cartId);

        //    //Assert
        //    Assert.AreEqual("CheckoutDetails", result.ViewName);
        //}
    }  
}

