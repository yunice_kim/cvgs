﻿using ConestogaVirtualGameStore.Framework.Entities;
using ConestogaVirtualGameStore.Web.Areas.Management.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CVGS.UnitTest
{
    [TestClass]
    public class GameControllerTest
    {
        //for employee
        [TestMethod]
        public void TestCreateNewGame()
        {
            // Arrange
            GameController controller = new GameController();
            Game game = new Game();

            game.Name = "Fortnite";
            game.Description = "Online video game developed by Epic Games";
            game.Price = 50;
            game.Quantity = 10;
            game.ReleaseDate = DateTime.Parse("2017-07-21");
            game.Supplier = "Epic Games";
            game.GameCategoryId = 1;
            game.GamePlatformCode = "XBox";

            // Act
            RedirectToRouteResult result = (RedirectToRouteResult)controller.Create(game);

            // Assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
        }

        [TestMethod]
        public void TestCreateGameWithNegativeQuanity()
        {
            // Arrange
            ConestogaVirtualGameStore.Web.Areas.Management.Controllers.GameController controller = new GameController();
            Game game = new Game();

            game.GameId = 1;
            game.Name = "Fortnite";
            game.Description = "Online video game developed by Epic Games";
            game.Price = 50;
            game.Quantity = -10;
            game.ReleaseDate = DateTime.Parse("2022-07-21");
            game.Supplier = "Epic Games";
            game.GameCategoryId = 1;
            game.GamePlatformCode = "XBox";

            // Act
            RedirectToRouteResult result = (RedirectToRouteResult)controller.Create(game);

            // Assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
        }

        //[TestMethod]
        //public void TestEditExistingGame()
        //{
        //    // Arrange
        //    GameController controller = new GameController();
        //    Game game = new Game();

        //    game.GameId = 1;
        //    //game.Name = "Fortnite [Edited]";
        //    //game.Description = "Online video game developed by Epic Games";
        //    game.Price = 50;
        //    game.Quantity = 10;
        //    //game.ReleaseDate = DateTime.Parse("2022-07-21");
        //    //game.Supplier = "Epic Games";
        //    game.GameCategoryId = 1;
        //    game.GamePlatformCode = "DS";

        //    // Act
        //    RedirectToRouteResult result = (RedirectToRouteResult)controller.Edit(game);

        //    // Assert
        //    Assert.AreEqual("Index", result.RouteValues["action"]);
        //}
        [TestMethod]

        public void TestWhenViewingDetailsOfNonExitentReviewReturn404()
        {
            //Arrange - prepare the controller
            GameController controller = new GameController();

            //Act - get non existent review with fake id -1
            var result = controller.Details(-1);

            //Assert - make sure the result is HttpNotFound (status=404)
            var prop = result.GetType().GetProperty("StatusCode");
            if (prop != null)
            {
                Assert.AreEqual(prop.GetValue(result), 404);
            }
            else
            {
                Assert.Fail();
            }
        }

        //create customer's site test
        [TestMethod]
        public void TestReviewAddingNewGameReviewSuccessfully()
        {
            // Arrange
            var controller = new ConestogaVirtualGameStore.Web.Controllers.GameController();
            Review review = new Review();
            review.Details = "Review 1 for this game";
            review.GameId = 1;
            review.Ratings = 0;

            // Act
            RedirectToRouteResult result = (RedirectToRouteResult)controller.CreateReview(review);

            // Assert
            Assert.AreEqual("Details", result.RouteValues["action"]);

        }

        [TestMethod]
        public void TestReviewAddingNewGameReviewWithNoDetails()
        {
            // Arrange
            var controller = new ConestogaVirtualGameStore.Web.Controllers.GameController();
            Review review = new Review();
            review.GameId = 1;
            review.Ratings = 4;

            // Act
            RedirectToRouteResult result = (RedirectToRouteResult)controller.CreateReview(review);

            // Assert
            Assert.AreEqual("Details", result.RouteValues["action"]);

        }

        [TestMethod]
        public void TestApproveReviewSuccess()
        {
            // Arrange
            GameController controller = new GameController();

            int gameId = 1;
            int reviewId = 1;

            // Act
            RedirectToRouteResult result = (RedirectToRouteResult)controller.Approve(gameId, reviewId);

            // Assert
            Assert.AreEqual("Details", result.RouteValues["action"]);
        }


        [TestMethod]

        public void TestReviewWhenDeletedByEmployeeSuccessfully()
        {
            // Arrange
            GameController controller = new ConestogaVirtualGameStore.Web.Areas.Management.Controllers.GameController();
            int gameId = 1;
            int reviewId = 1;

            // Act
            RedirectToRouteResult result = (RedirectToRouteResult)controller.DeleteReview(gameId, reviewId);

            // Assert
            Assert.AreEqual("Details", result.RouteValues["action"]);
        }

    }
}
