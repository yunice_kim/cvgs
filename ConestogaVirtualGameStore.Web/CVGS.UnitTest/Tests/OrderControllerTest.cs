﻿using ConestogaVirtualGameStore.Framework.Entities;
using ConestogaVirtualGameStore.Web.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;

namespace CVGS.UnitTest.Tests
{

    [TestClass]
    public class OrderControllerTest
    {
        [TestMethod]
        public void TestDetails()
        {
            //Arrange
            var controller = new OrderController();
            int itemId = 1;
            int accountId = 2;

            //Act
            ViewResultBase result = (ViewResultBase)controller.Details(itemId, accountId);

            //Assert
            Assert.AreEqual("", result.ViewName);
        }

       

        //[TestMethod]
        //public void TestProceed()
        //{
        //need to use accoundId from session
        //    //Arrange
        //    var controller = new OrderController();

        //    int cartId = 1;
        //    int accountId = 1;

        //    //Act
        //    ViewResultBase result = (ViewResultBase)controller.Checkout(accountId, cartId);

        //    //Assert
        //    Assert.AreEqual("CheckoutDetails", result.ViewName);
        //}
    }  
}

