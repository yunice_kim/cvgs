﻿using ConestogaVirtualGameStore.Framework.Entities;
using ConestogaVirtualGameStore.Web.Controllers;
using ConestogaVirtualGameStore.Web.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Web.Mvc;

namespace CVGS.UnitTest
{
    [TestClass]
    public class PersonControllerTest
    {
        Account account;
        Person person;
        Address address;
        readonly ProfileViewModel profile;

        //[TestMethod]
        //public void TestValidProfileEdits()
        //{
        //    //Arrange
        //    PersonController controller = new PersonController();
        //    account = new Account();
        //    person = new Person();
        //    address = new Address();
        //    profile.Account = account;
        //    profile.Person = person;
        //    profile.Address = address;
        //    profile.Person.PersonId = 1;
        //    profile.Account.AccountId = 1;
        //    profile.Account.PersonId = 1;
        //    profile.Address.AddressId = 1;
        //    profile.Address.AddressType = AddressType.Billing;
        //    profile.Address.PersonId = 1;
        //    profile.Person.FirstName = "Bob";
        //    profile.Person.LastName = "Jones";
        //    profile.Person.Addresses.Add(address);
        //    profile.Address.Street = "123 Street st";
            

        //    //Act
        //    //RedirectToRouteResult result = (RedirectToRouteResult)controller.Login(account);
        //    ViewResultBase result = (ViewResultBase)controller.Edit(profile);

        //    //Assert
        //    //Assert.AreEqual("Login", result.RouteValues["action"]);
        //    Assert.AreEqual("Index", result.ViewName);
        //}

        //[TestMethod]
        //public void TestBadPostalCodeEdit()
        //{
        //    //Arrange
        //    PersonController controller = new PersonController();
        //    account = new Account();
        //    person = new Person();
        //    address = new Address();
        //    profile.Account = account;
        //    profile.Person = person;
        //    profile.Address = address;
        //    profile.Person.PersonId = 1;
        //    profile.Account.AccountId = 1;
        //    profile.Account.PersonId = 1;
        //    profile.Address.AddressId = 1;
        //    profile.Address.AddressType = AddressType.Billing;
        //    profile.Address.PersonId = 1;
        //    profile.Person.FirstName = "Bob";
        //    profile.Person.LastName = "Jones";
        //    profile.Person.Addresses.Add(address);
        //    profile.Address.Street = "123 Street st";
        //    profile.Person.PhoneNumber = "abc";


        //    //Act
        //    //RedirectToRouteResult result = (RedirectToRouteResult)controller.Login(account);
        //    ViewResultBase result = (ViewResultBase)controller.Edit(profile);

        //    //Assert
        //    //Assert.AreEqual("Login", result.RouteValues["action"]);
        //    Assert.AreEqual("Edit", result.ViewName);
        //}

        //[TestMethod]
        //public void TestBadPhoneNumberProfileEdit()
        //{
        //    //Arrange
        //    PersonController controller = new PersonController();
        //    account = new Account();
        //    person = new Person();
        //    address = new Address();
        //    profile.Account = account;
        //    profile.Person = person;
        //    profile.Address = address;
        //    profile.Person.PersonId = 1;
        //    profile.Account.AccountId = 1;
        //    profile.Account.PersonId = 1;
        //    profile.Address.AddressId = 1;
        //    profile.Address.AddressType = AddressType.Billing;
        //    profile.Address.PersonId = 1;
        //    profile.Person.FirstName = "Bob";
        //    profile.Person.LastName = "Jones";
        //    profile.Person.Addresses.Add(address);
        //    profile.Address.Street = "123 Street st";
        //    profile.Address.PostalCode = "abc";


        //    //Act
        //    //RedirectToRouteResult result = (RedirectToRouteResult)controller.Login(account);
        //    ViewResultBase result = (ViewResultBase)controller.Edit(profile);

        //    //Assert
        //    //Assert.AreEqual("Login", result.RouteValues["action"]);
        //    Assert.AreEqual("Edit", result.ViewName);
        //}
    }
}
