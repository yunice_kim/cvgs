﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConestogaVirtualGameStore.Framework.Entities;
using ConestogaVirtualGameStore.Web.Controllers;
using System.Web.Mvc;

namespace CVGS.UnitTest.Tests
{
    /// <summary>
    /// Summary description for ProfileControllerTest
    /// </summary>
    [TestClass]
    public class ProfileControllerTest
    {
        //Preference preference;

        [TestMethod]
        public void TestCreatePreferenceForUser()
        {
            Preference preference = new Preference();
            ProfileController controller = new ProfileController();
            preference.GameCategoryId = 1;
            preference.GamePlatformCode = "DS";
            preference.AccountId = 1;

            //Act
            RedirectToRouteResult result = (RedirectToRouteResult)controller.CreatePreference(preference);
            //Assert
            Assert.AreEqual("Index", result.RouteValues["action"]);

        }
        [TestMethod]
        public void TestEditPreferenceForUser()
        {
            Preference preference = new Preference();
            ProfileController controller = new ProfileController();
            preference.GameCategoryId = 3;
            preference.GamePlatformCode = "PC";
            preference.PreferenceId = 1;
            preference.AccountId = 1;
            
            
            

            //Act
            RedirectToRouteResult result = (RedirectToRouteResult)controller.EditPreferences(preference);
            //Assert
            Assert.AreEqual("Index", result.RouteValues["action"]);

        }

    }
}
