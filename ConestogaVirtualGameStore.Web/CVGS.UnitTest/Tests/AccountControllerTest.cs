﻿using ConestogaVirtualGameStore.Framework.Entities;
using ConestogaVirtualGameStore.Web.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Web.Mvc;

namespace CVGS.UnitTest
{
    [TestClass]
    public class AccountControllerTest
    {
        Account account;
        Person person;

        [TestMethod]
        public void TestLoginWithNonExistUser()
        {
            //Arrange
            AccountController controller = new AccountController();
            account = new Account
            {
                UserName = "test",
                Password = "test111"
            };

            //Act
            //RedirectToRouteResult result = (RedirectToRouteResult)controller.Login(account);
            ViewResultBase result = (ViewResultBase)controller.Login(account);

            //Assert
            //Assert.AreEqual("Login", result.RouteValues["action"]);
            Assert.AreEqual("Login", result.ViewName);
        }

        [TestMethod]
        public void TestLoginWithWrongPassword()
        {
            //Arrange
            AccountController controller = new AccountController();
            account = new Account
            {
                UserName = "test1",
                Password = "test1110"
            };

            //Act
            //RedirectToRouteResult result = (RedirectToRouteResult)controller.Login(account);
            ViewResultBase result = (ViewResultBase)controller.Login(account);

            //Assert
            //Assert.AreEqual("Login", result.RouteValues["action"]);
            Assert.AreEqual("Login", result.ViewName);
        }

        //[TestMethod]
        //public void TestLoginViewWithValidUser()
        //{
        //    //Arrange
        //    AccountController controller = new AccountController();
        //    account = new Account();
        //    account.UserName = "testuser1";
        //    account.Password = "111qqq";

        //    //Act
        //    //RedirectToRouteResult result = (RedirectToRouteResult)controller.Login(account);
        //    ViewResultBase result = (ViewResultBase)controller.Login(account);

        //    //Assert
        //    //Assert.AreEqual("Login", result.RouteValues["action"]);
        //    Assert.AreEqual("Index", result.ViewName);
        //}


        [TestMethod]
        public void TestSignupWithDuplicatedUser()
        {
            //Arrange
            AccountController controller = new AccountController();
            account = new Account
            {
                UserName = "test1"
            };
            person = new Person();


            //Act
            ViewResultBase result = (ViewResultBase)controller.Signup(account, person);

            //Assert
            Assert.AreEqual("Signup", result.ViewName);
        }
    }
}
