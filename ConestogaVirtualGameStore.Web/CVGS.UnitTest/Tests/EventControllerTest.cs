﻿using ConestogaVirtualGameStore.Framework.Entities;
using ConestogaVirtualGameStore.Web.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Web.Mvc;

namespace CVGS.UnitTest
{
    [TestClass]
    public class EventControllerTest
    {
        //Account account;
        //Person person;

        [TestMethod]
        public void TestListAllEvent()
        {
            //Arrange
            EventController controller = new EventController();

            //Act
            ViewResultBase result = (ViewResultBase)controller.Index();

            //Assert
            Assert.AreEqual("Index", result.ViewName);
        }

        [TestMethod]
        public void TestEventDetails()
        {
            //Arrange
            EventController controller = new EventController();
            //Event testEvent = new Event();
            //testEvent.EventId = 1;
            //Act
            ViewResultBase result = (ViewResultBase)controller.Details(1);

            //Assert
            Assert.AreEqual("Details", result.ViewName);
        }
    }
}