﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ConestogaVirtualGameStore.Framework.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConestogaVirtualGameStore.Web.Controllers;



namespace CVGS.UnitTest.Tests
{
    [TestClass]
    public class EventRegistrationControllerTest
    {
        
        [TestMethod]
        public void TestCreateEventRegister()
        {
            EventRegister eventRegister = new EventRegister();
            EventRegisterController controller = new EventRegisterController();
            eventRegister.AccountId = 3;
            eventRegister.EventId = 1;
            
            
            //Act
            RedirectToRouteResult result = (RedirectToRouteResult)controller.Create(eventRegister);
            //Assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
        }

    }
}
