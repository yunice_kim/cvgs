﻿using ConestogaVirtualGameStore.Framework.Entities;
using ConestogaVirtualGameStore.Web.Controllers;
using ConestogaVirtualGameStore.Web.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;

namespace CVGS.UnitTest.Tests
{
    [TestClass]
    public class FriendControllerTest
    {
        FriendController controller = new FriendController();
        private GameStoreContext db = new GameStoreContext();
        [TestMethod]
        public void TestSearchFriend()
        {
            //Arrange
            string userName = "test2";
            
            //Act
            RedirectToRouteResult result = (RedirectToRouteResult)controller.SearchFriend(userName);

            //Assert
            Assert.AreEqual("FriendUser", result.RouteValues["action"]);

        }

        //[TestMethod]
        //public void TestGetFriendWishlist()
        //{
        //    //Arrange
        //    string userName = "test2";
        //    Wishlist wishList = new Wishlist();
            

        //    //Act
        //    RedirectToRouteResult result = (RedirectToRouteResult)controller.GetFriendWishlist(userName);

        //    //Assert
        //    Assert.AreEqual("FriendUser", result.RouteValues["action"]);

        //}
    }
}
