﻿using ConestogaVirtualGameStore.Framework.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;

namespace ConestogaVirtualGameStore.Web.Areas.Management.Models
{
    public class OrderDetails
    {
        public Order Order { get; set; }
        public Game Game { get; set; }
        public List<LineItem> LineItem { get; set; }

    }

     
}