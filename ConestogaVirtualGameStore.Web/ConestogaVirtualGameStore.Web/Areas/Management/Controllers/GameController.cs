﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ConestogaVirtualGameStore.Framework.Entities;
using Microsoft.Reporting.WebForms;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

namespace ConestogaVirtualGameStore.Web.Areas.Management.Controllers
{
    [Authorize(Roles = "Employee")]
    public class GameController : Controller
    {
        private readonly GameStoreContext db = new GameStoreContext();

        // GET: Management/Game
        public ActionResult Index()
        {
            return View(db.Game.ToList());
        }

        // GET: Management/Game/Details/5
        public ActionResult Details(int? id)
        {
            TempData["averageRating"] = null;
            if (id == null)
            {
                if(TempData["gameId"] != null)
                {
                    id = Convert.ToInt32(TempData["gameId"]);
                }
                else
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
            Game game = db.Game.Find(id);

            if (game == null)
            {
                return HttpNotFound();
            }

            TempData["gameName"] = game.Name;

            IEnumerable<Review> reviews = new List<Review>();
            reviews = db.Review.Where(x => x.GameId == id).ToList();
            ViewData["reviews"] = reviews;
            double averageRating = CalculateAverageRating(id);
            TempData["averageRating"] = averageRating;
            return View(game);
        }

        [HttpPost]
        public ActionResult Approve(int gameId, int reviewId)
        {
            // no need to check model state
            // because the user did not enter any manual values

            Review review = db.Review.Find(reviewId);
            // update the review
            db.Entry(review).State = EntityState.Modified;
            review.Approved = true;
            db.SaveChanges();
            TempData["gameId"] = gameId;
            return RedirectToAction("Details");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteReview(int reviewId, int gameId)
        {
            Review review = db.Review.Find(reviewId);
            db.Review.Remove(review);
            db.SaveChanges();
            TempData["gameId"] = gameId;
            return RedirectToAction("Details");
        }

        // GET: Management/Game/Create
        public ActionResult Create()
        {
            ViewBag.GameCategoryId = new SelectList(db.GameCategory, "GameCategoryId", "CategoryName");
            ViewBag.GamePlatformCode = new SelectList(db.GamePlatform, "GamePlatformCode", "Name");
            return View();
        }

        // POST: Management/Game/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Game game)
        {
            if (ModelState.IsValid)
            {
                db.Game.Add(game);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.GameCategoryId = new SelectList(db.GameCategory, "GameCategoryId", "CategoryName");
            ViewBag.GamePlatformCode = new SelectList(db.GamePlatform, "GamePlatformCode", "Name");
            return View(game);
        }

        // GET: Management/Game/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Game game = db.Game.Find(id);
            if (game == null)
            {
                return HttpNotFound();
            }
            ViewBag.GameCategoryId = new SelectList(db.GameCategory, "GameCategoryId", "CategoryName", game.GameCategoryId);
            ViewBag.GamePlatformCode = new SelectList(db.GamePlatform, "GamePlatformCode", "Name", game.GamePlatformCode);
            TempData["gameName"] = game.Name;
            return View(game);
        }

        // POST: Management/Game/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Game game)
        {
            var allErrors = ModelState.Values.SelectMany(x => x.Errors);

            if (ModelState.IsValid)
            {
                db.Entry(game).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.GameCategoryId = new SelectList(db.GameCategory, "GameCategoryId", "CategoryName");
            ViewBag.GamePlatformCode = new SelectList(db.GamePlatform, "GamePlatformCode", "Name");
            return View(game);
        }

        // GET: Management/Game/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Game game = db.Game.Find(id);
            if (game == null)
            {
                return HttpNotFound();
            }

            return View(game);
        }

        // POST: Management/Game/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Game game = db.Game.Find(id);
            db.Game.Remove(game);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        readonly ConestogaVirtualGameStore.Web.Areas.Management.Reports.Report ds =
            new ConestogaVirtualGameStore.Web.Areas.Management.Reports.Report();
        public ActionResult GameReport()
        {
            ReportViewer reportViewer = new ReportViewer
            {
                ProcessingMode = ProcessingMode.Local,
                SizeToReportContent = true,
                Width = Unit.Percentage(10000),
                Height = Unit.Percentage(10000)
            };

            var connectionString = ConfigurationManager.
                ConnectionStrings["GameStoreContext"].ConnectionString;

            SqlConnection conx = new SqlConnection(connectionString);
            SqlDataAdapter adp = new SqlDataAdapter("SELECT * FROM Game", conx);

            adp.Fill(ds, ds.Game.TableName);

            
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Areas\Management\Reports\GameReport.rdlc";
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("GameDataSet", ds.Tables[1]));

            ViewBag.ReportViewer = reportViewer;

            return View();
        }

        public double CalculateAverageRating(int? gameId)
        {
            if (db.Review.Where(a => a.GameId == gameId && a.Approved == true).Count() == 0)
            {
                return 0;
            }
            int star5 = db.Review.Where(a => a.GameId == gameId && a.Ratings == 5 && a.Approved == true).Count();
            int star4 = db.Review.Where(a => a.GameId == gameId && a.Ratings == 4 && a.Approved == true).Count();
            int star3 = db.Review.Where(a => a.GameId == gameId && a.Ratings == 3 && a.Approved == true).Count();
            int star2 = db.Review.Where(a => a.GameId == gameId && a.Ratings == 2 && a.Approved == true).Count();
            int star1 = db.Review.Where(a => a.GameId == gameId && a.Ratings == 1 && a.Approved == true).Count();

            double averageRating = 0;

            averageRating = (double)(5 * star5 + 4 * star4 + 3 * star3 + 2 * star2 + 1 * star1) / (star1 + star2 + star3 + star4 + star5);

            averageRating = Math.Round(averageRating, 1);

            return averageRating;
        }

    }
}
