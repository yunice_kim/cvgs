﻿using ConestogaVirtualGameStore.Framework.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

namespace ConestogaVirtualGameStore.Web.Areas.Management.Controllers
{
    [Authorize(Roles = "Employee")]
    public class EventController : Controller
    {
        private GameStoreContext db = new GameStoreContext();

        // GET: Management/Event
        public ActionResult Index()
        {
            return View(db.Event.ToList());
            //return View();
        }

        // GET: Management/Event/Create
        public ActionResult Create()
        {
            //ViewBag.GameCategoryId = new SelectList(db.GameCategory, "GameCategoryId", "CategoryName");
            //ViewBag.GamePlatformCode = new SelectList(db.GamePlatform, "GamePlatformCode", "Name");
            return View();
        }

        // GET: Management/Event/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event eventSelected = db.Event.Find(id);
            if (eventSelected == null)
            {
                return HttpNotFound();
            }
            TempData["eventName"] = eventSelected.Title;

            return View(eventSelected);
        }

        // GET: Management/Event/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event eventSelected = db.Event.Find(id);
            if (eventSelected == null)
            {
                return HttpNotFound();
            }
            //ViewBag.GameCategoryId = new SelectList(db.GameCategory, "GameCategoryId", "CategoryName");
            //ViewBag.GamePlatformCode = new SelectList(db.GamePlatform, "GamePlatformCode", "Name");
            TempData["eventName"] = eventSelected.Title;
            return View(eventSelected);
        }
        // GET: Management/Game/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event eventSelected = db.Event.Find(id);
            if (eventSelected == null)
            {
                return HttpNotFound();
            }
            TempData["eventName"] = eventSelected.Title;
            return View(eventSelected);
        }


        // POST: Management/Event/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Event eventEntered)
        {
            if (ModelState.IsValid)
            {
                db.Event.Add(eventEntered);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(eventEntered);
        }

        // POST: Management/Event/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Event eventModified)
        {
            if (ModelState.IsValid)
            {
                db.Entry(eventModified).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(eventModified);
        }

        // POST: Management/Event/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Event eventSelected = db.Event.Find(id);
            db.Event.Remove(eventSelected);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        ConestogaVirtualGameStore.Web.Areas.Management.Reports.Report ds =
            new ConestogaVirtualGameStore.Web.Areas.Management.Reports.Report();
        public ActionResult EventReport()
        {
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;
            reportViewer.Width = Unit.Percentage(1000);
            reportViewer.Height = Unit.Percentage(1000);

            var connectionString = ConfigurationManager.
                ConnectionStrings["GameStoreContext"].ConnectionString;

            SqlConnection conx = new SqlConnection(connectionString);
            SqlDataAdapter adp = new SqlDataAdapter("SELECT * FROM Event", conx);

            adp.Fill(ds, ds.Event.TableName);

            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Areas\Management\Reports\EventReport.rdlc";
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("EventDataSet", ds.Tables[0]));


            ViewBag.ReportViewer = reportViewer;

            return View();
        }
    }
}