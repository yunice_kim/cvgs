﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ConestogaVirtualGameStore.Framework.Entities;
using ConestogaVirtualGameStore.Web.Areas.Management.Models;

namespace ConestogaVirtualGameStore.Web.Areas.Management.Controllers
{
    public class OrdersController : Controller
    {
        private readonly GameStoreContext db = new GameStoreContext();

        // GET: Management/Orders
        public ActionResult Index()
        {
            var accountId = Convert.ToInt32(Session["accountId"]);
            var orderId = Convert.ToInt32(Session["orderId"]);
            var order = db.Order.Include(o => o.Account).Include(o => o.Address);
            return View(order.ToList().OrderBy(x => x.OrderDate));
        }

        // GET: Management/Orders/Details/5

        public ActionResult Details(int? orderId, int? accountId)
        {
            accountId = Convert.ToInt32(Session["accountId"]);
            

            /*if (accountId == null)
            {
                TempData["message"] = "Need to login in order to see the details of Order";
                return RedirectToAction("Login", "Account");
            }
            else
            { */
                //1. get the total of current order
            var order = db.Order.Find(orderId);
            TempData["OrderStatus"] = order.OrderStatus;
            ViewBag.Order = order;
            ViewBag.OrderId = orderId;
            
            //TempData["total"] = order.OrderTotal;

            //2. retrieve the line item list with the order Id that are already ordered
            var lineItemList = db.LineItem.Where(x => x.OrderId == orderId).ToList();

                //3. get the used checkout information for billing address and credit card 
            var address = db.Address.Where(x => x.AddressId == order.AddressId).SingleOrDefault();
                //var creditCard = db.CreditCard.Where(x => x.CreditCardId == order.CreditCardId).SingleOrDefault();
                //3.1 save address and credit card information to the ViewBag to send them to the Details view
            ViewBag.Address = address;
                //ViewBag.CreditCard = creditCard;

                //5. return the lineItemList to the Details view
            return View(lineItemList.ToList());
            //}


        }
        // GET: Management/Orders/Edit/5
        public ActionResult Edit(int? id)
        {
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Order.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            
            
            ViewBag.AccountId = new SelectList(db.Account, "AccountId", "UserName", order.AccountId);
            ViewBag.AddressId = new SelectList(db.Address, "AddressId", "Street", order.AddressId);
            ViewBag.CreditCardId = new SelectList(db.CreditCard, "CreditCardId", "Cardholder", order.CreditCardId);
            return View(order);
        }

        // POST: Management/Orders/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OrderId,OrderStatus,ProcessedDate,OrderDate,OrderTotal,AccountId,CreditCardId,AddressId")] Order order)
        {

            if (ModelState.IsValid)
            {

                order.ProcessedDate = DateTime.Now;
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            
            TempData["OrderStatus"] = order.OrderStatus;
            ViewBag.AccountId = new SelectList(db.Account, "AccountId", "UserName", order.AccountId);
            ViewBag.AddressId = new SelectList(db.Address, "AddressId", "Street", order.AddressId);
            ViewBag.CreditCardId = new SelectList(db.CreditCard, "CreditCardId", "Cardholder", order.CreditCardId);
            return View(order);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
