﻿using ConestogaVirtualGameStore.Framework.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConestogaVirtualGameStore.Web.Controllers
{
    public class HomeController : Controller
    {
        private GameStoreContext db = new GameStoreContext();

        public ActionResult Index()
        {
            var accountId = Convert.ToInt32(Session["accountId"]);

            if (TempData["logout"] == null || TempData["logout"].ToString() == "logout") 
            {
                return View();
            }
            else if (accountId == 0)
            {
                TempData["message"] = "Session expired. Login please";
                return RedirectToAction("Login", "Account");
            }
            else
            {
                var cart = db.Cart.SingleOrDefault(x => x.AccountId == accountId);
                var lineItemList = db.LineItem.Where(x => x.CartId == cart.CartId).ToList();
                TempData["lineItemCount"] = lineItemList.Count;
                return View();
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}