﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using ConestogaVirtualGameStore.Framework.Entities;
using ConestogaVirtualGameStore.Web.Models;

namespace ConestogaVirtualGameStore.Web.Controllers
{
    [Authorize(Roles = "Customer")]
    public class PersonController : Controller
    {
        private readonly GameStoreContext db = new GameStoreContext();

        // GET: Person
        public ActionResult Index()
        {
            CountLineItemInCart();
            return View(db.Person.ToList());
        }

        // GET: Person/Details/5
        public ActionResult Details(int? id)
        {
            id = Convert.ToInt32(Session["accountId"]);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.Person.Find(id);
            Account account = db.Account.FirstOrDefault(a => a.PersonId == id);
            Address billingAddress = db.Address.FirstOrDefault(a => a.PersonId == id && a.AddressType == AddressType.Billing);
            Address shippingAddress = db.Address.FirstOrDefault(a => a.PersonId == id && a.AddressType == AddressType.Shipping);

            ProfileViewModel profile = new ProfileViewModel
            {
                Person = person,
                Account = account,
                BillingAddress = billingAddress,
                ShippingAddress = shippingAddress,
                UseSameAddress = false
            };
            if (billingAddress.Street == shippingAddress.Street)
            {
                profile.UseSameAddress = true;
            }
            if (person == null)
            {
                return HttpNotFound();
            }

            CountLineItemInCart();

            return View(profile);
        }

        private void CountLineItemInCart()
        {
            var accountId = Convert.ToInt32(Session["accountId"]);
            var latestCart = db.Cart.Where(x => x.AccountId == accountId).OrderByDescending(x => x.CreatedOn).FirstOrDefault();
            if (latestCart != null)
            {
                var lineItemList = db.LineItem.Where(x => x.CartId == latestCart.CartId).ToList();

                //if the first line item's Order id is null, all list item for the cart id has all null for order id
                //and it means the cart isn't proceeded yet
                //so in order to show the count of line item list beside the Cart
                if (lineItemList[0].OrderId == null)
                {
                    TempData["lineItemCount"] = lineItemList.Count;
                }
                else
                {
                    TempData["lineItemCount"] = 0;
                }
            }
        }

        // GET: Person/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Person/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PersonId,FirstName,LastName,DateOfBirth,PhoneNumber,Email,Gender")] Person person)
        {
            if (ModelState.IsValid)
            {
                db.Person.Add(person);
                db.SaveChanges();
                CountLineItemInCart();
                return RedirectToAction("Index");
            }

            CountLineItemInCart();
            return View(person);
        }

        // GET: Person/Edit/5
        public ActionResult Edit(int? id)
        {
            id = Convert.ToInt32(Session["accountId"]);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.Person.Find(id);
            Account account = db.Account.FirstOrDefault(a => a.PersonId == id);
            Address billingAddress = db.Address.FirstOrDefault(a => a.PersonId == id && a.AddressType == AddressType.Billing);
            Address shippingAddress = db.Address.FirstOrDefault(a => a.PersonId == id && a.AddressType == AddressType.Shipping);

            ProfileViewModel profile = new ProfileViewModel
            {
                Person = person,
                Account = account,
                BillingAddress = billingAddress,
                ShippingAddress = shippingAddress,
                UseSameAddress = false
            };
            if (person == null)
            {
                return HttpNotFound();
            }

            CountLineItemInCart();

            return View(profile);
        }

        // POST: Person/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProfileViewModel profile)
        {
            if(ModelState.IsValid)
            {
                if (profile.Person.PhoneNumber != null)
                {
                    Regex phoneRegex = new Regex(@"^\(?\d{3}\)?-? *\d{3}-? *-?\d{4}$");
                    if (!phoneRegex.IsMatch(profile.Person.PhoneNumber))
                    {
                        ModelState.AddModelError("Person.PhoneNumber", "Invalid Phone Number");
                        CountLineItemInCart();
                        return View(profile);
                    }
                }

                if (profile.BillingAddress.PostalCode != null)
                {
                    Regex postalCodeRegex = new Regex(@"^[A-z][0-9][A-z] ?[0-9][A-z][0-9]$");
                    if(!postalCodeRegex.IsMatch(profile.BillingAddress.PostalCode))
                    {
                        ModelState.AddModelError("BillingAddress.PostalCode", "Invalid Postal Code");
                        CountLineItemInCart();

                        return View(profile);
                    }

                }

                if (profile.UseSameAddress)
                {
                    profile.CopyAddresses();
                }

                db.Entry(profile.Person).State = EntityState.Modified;
                //db.Entry(profile.Account).State = EntityState.Modified;
                db.Entry(profile.BillingAddress).State = EntityState.Modified;
                db.Entry(profile.ShippingAddress).State = EntityState.Modified;
                db.SaveChanges();

                CountLineItemInCart();
                return RedirectToAction("Index","Profile");
            }

            CountLineItemInCart();
            return View(profile);
        }

        // GET: Person/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.Person.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: Person/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Person person = db.Person.Find(id);
            db.Person.Remove(person);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
