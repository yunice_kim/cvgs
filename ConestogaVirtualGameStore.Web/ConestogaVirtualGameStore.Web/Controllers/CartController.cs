﻿using ConestogaVirtualGameStore.Framework.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace ConestogaVirtualGameStore.Web.Controllers
{
    public class CartController : Controller
    {
        private readonly GameStoreContext db = new GameStoreContext();

        // GET: Cart/ViewCart
        public ActionResult ViewCart()
        {
            var accountId = Convert.ToInt32(Session["accountId"]);

            if(accountId == 0)
            {
                TempData["message"] = "Need to login in order to see the details of item in the Cart";
                return RedirectToAction("Login", "Account");
            }
            else
            {
                //in order to show the count of line item beside the Cart
                var lineItemList = CountLineItemInCart(accountId);

                return View(lineItemList.ToList());
            }
        }

        private List<LineItem> CountLineItemInCart(int accountId)
        {
            //1. get latest cart for the current account
            var latestCart = db.Cart.Where(x => x.AccountId == accountId).OrderByDescending(x => x.CreatedOn).FirstOrDefault();
            List<LineItem> lineItemList = new List<LineItem>();
            if (latestCart != null)
            {
                lineItemList = db.LineItem.Where(x => x.CartId == latestCart.CartId).ToList();

                //2. if the first line item's Order id is null, all list item for the cart id has all null for order id
                //and it means the cart isn't proceeded yet
                //so in order to show the count of line item list beside the Cart
                if (lineItemList[0].OrderId == null)
                {
                    TempData["lineItemCount"] = lineItemList.Count;
                    //ViewData["lineItemList"] = lineItemList;
                    TempData["cartId"] = latestCart.CartId;
                    double total = 0.0;
                    List<Game> changedItemList = new List<Game>();
                    //check each line item's quantity is available to order from Game table
                    foreach (var lineItem in lineItemList)
                    {
                        total += (lineItem.Price * lineItem.Quantity);

                        //retrieve game quantity for each line item
                        var game = db.Game.Where(x => x.GameId == lineItem.GameId).FirstOrDefault();
                        //if the game quantity(onHand) is less than the quantity of line item in Cart
                        if (game.Quantity < lineItem.Quantity)
                        {
                            //add the line item to the changed item list to show the name in the view
                            //to let customer notice the avaiable quantity is changed for each item
                            changedItemList.Add(game);
                        }
                    }

                    TempData["total"] = total;
                    ViewData["changedItemList"] = changedItemList;
                }
                else
                {
                    TempData["lineItemCount"] = 0;
                }

            }
            else
            {
                TempData["lineItemCount"] = 0;
            }
            return lineItemList;

        }

        // GET: Cart/Details/5
        public ActionResult Details(int? id, int? accountId)
        {
            if (accountId == null)
            {

                TempData["message"] = "Need to login in order to see the details of item in the Cart";
                return RedirectToAction("Login", "Account");
            }
            else
            {
                LineItem lineItem = db.LineItem.Find(id);
                Game game = db.Game.SingleOrDefault(x => x.GameId == lineItem.GameId);
                TempData["selectedQuantity"] = lineItem.Quantity;
                TempData["itemName"] = game.Name;
                TempData["lineItemId"] = lineItem.LineItemId;
                TempData["option"] = lineItem.Option;
                
                var lineItemList = db.LineItem.Where(x => x.CartId == lineItem.CartId).ToList();
                TempData["lineItemCount"] = lineItemList.Count;
                return View(game);
            }
        }

        // GET: Cart/Checkout
        public ActionResult Checkout(int? accountId, int? cartId)
        {
            //accountId = Convert.ToInt32(Session["accountId"]);

            if (accountId == null)
            {
                TempData["message"] = "Need to login in order to checkout all items in the Cart";
                return RedirectToAction("Login", "Account");
            }
            else
            {
                //1. get a latest card number
                var account = db.Account.Where(x => x.AccountId == accountId).FirstOrDefault();
                var lastUsedCreditCardId = account.LastUsedCreditCard;
                ViewBag.LastUsedCreditCardId = lastUsedCreditCardId;

                //2. get the added credit card list from the credit card table
                var creditCardList = db.CreditCard.Where(x => x.AccountId == accountId).ToList();
                ViewBag.CreditCardList = creditCardList;
                
                //3. get a latest billing address
                var lastUsedAddressId = account.LastUsedAddressId;
                //4. get the address list with the person id of the person after getting the person with the account id
                var addressList = db.Address.Where(x => x.PersonId == account.PersonId).ToList();
                //5. at least one field of the address is null, it's not a valid address
                //1) use dictionary type to keep the result of the entity(address)'s null status and the address id to get the result
                Dictionary<int, bool> validAddressDic = new Dictionary<int, bool>();
                bool allNotNull = true;
                foreach (Address address in addressList)
                {
                        
                    if (!address.GetType().GetProperties().All(p => p.GetValue(address) != null))
                    {
                        allNotNull = false;
                    }
                    //2)all properties of Address are not null, the address id has True,
                    //otherwise, the address id has False which means the address is not valid
                    //so show the link to add valid address in the checkoutdetails view
                    validAddressDic.Add(address.AddressId, address.GetType().GetProperties().All(p => p.GetValue(address) != null));
                }
                //6. in order to show message of invalid address
                ViewBag.allNotNull = allNotNull;
                //7. in order to show radio button when the address id is valid
                ViewBag.validAddressDic = validAddressDic;
                ViewBag.LastUsedAddressId = lastUsedAddressId;
                ViewBag.AddressList = addressList;

                //8. get the list of line item on Cart
                var lineItemList = db.LineItem.Where(x => x.CartId == cartId).ToList();
                double total = 0.0;
                foreach(var lineItem in lineItemList)
                {
                    total += (lineItem.Price * lineItem.Quantity);
                }

                TempData["total"] = total;

                //9. count the line item on Cart and set it on TempData to show the number beside the Cart
                TempData["lineItemCount"] = lineItemList.Count;
                TempData["cartId"] = cartId;

                return View("CheckoutDetails", lineItemList.ToList());
            }
        }

        // POST: Cart/AddItem
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddItem(Game game, string option, int? accountId)
        {
            if (accountId != 0 && accountId != null)
            {
                int selectedQuantity = game.Quantity;

                var selectedGame = db.Game.SingleOrDefault(x => x.GameId == game.GameId);
                //check if the game exist on the table
                if (selectedGame != null)
                {
                    //1. check if there is cart for current account id
                    var latestCart = db.Cart.Where(x => x.AccountId == accountId).OrderByDescending(x => x.CreatedOn).FirstOrDefault();
                    //2. create new line item
                    LineItem item = new LineItem();

                    if (latestCart != null)
                    {
                        var lineItemListInCart = db.LineItem.Where(x => x.CartId == latestCart.CartId).ToList();
                        //if the line items for this cart are not proceeded yet
                        if (lineItemListInCart[0].OrderId == null)
                        {
                            //use current cart id as existing card id
                            item.CartId = latestCart.CartId;
                            //to send to the view for the cartId
                            TempData["cartId"] = latestCart.CartId;

                            //if the line item already exist in the db,
                            //retrieve all list with the game id,
                            var lineItemList = db.LineItem.Where(x => x.GameId == selectedGame.GameId && x.OrderId == null && x.CartId == latestCart.CartId).ToList();

                            if (lineItemList.Count != 0)
                            {
                                //then check the account id in the list to find the right line item with the account Id
                                foreach (var lineItem in lineItemList)
                                {
                                    if (lineItem.GameId == selectedGame.GameId)
                                    {
                                        //check the line item's option is physical copy

                                        if(lineItem.Option == "Physical" && option == "Physical")
                                        {
                                            //just add the number of line item in the existing number of line item in the db
                                            lineItem.Quantity += selectedQuantity;
                                            db.Entry(lineItem).State = EntityState.Modified;
                                            db.SaveChanges();
                                            break;
                                        }
                                        else if ((lineItem.Option == "Digital" && option == "Physical") 
                                            || (lineItem.Option == "Physical" && option == "Digital"))
                                        {
                                            //add the item into line item list with Option of Physical
                                            //cartId of line item is already set in the if statement above
                                            item.GameId = selectedGame.GameId;
                                            item.Quantity = selectedQuantity;
                                            item.Price = selectedGame.Price;
                                            item.Option = option;
                                            db.LineItem.Add(item);
                                            db.SaveChanges();
                                        }
                                        //else if (lineItem.Option == "Physical" && option == "Digital")
                                        //{
                                        //    //add the item into line item list with Option of Digital
                                        //    //cartId of line item is already set in the if statement above
                                        //    item.GameId = selectedGame.GameId;
                                        //    item.Quantity = selectedQuantity;
                                        //    item.Price = selectedGame.Price;
                                        //    item.Option = option;
                                        //    db.LineItem.Add(item);
                                        //    db.SaveChanges();
                                        //}
                                        else if (lineItem.Option == "Digital" && option == "Digital")
                                        {
                                            //don't do anything. digital copy is always 1.
                                            TempData["messageForDigital"] = "Already added as Digital copy";
                                            return RedirectToAction("Details", "Game", new { id = game.GameId});
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //or fill line item with cart info and save to db
                                //cartId of line item is already set in the if statement above
                                item.GameId = selectedGame.GameId;
                                item.Quantity = selectedQuantity;
                                item.Price = selectedGame.Price;
                                item.Option = option;
                                db.LineItem.Add(item);
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            //line item has the orderId which means it's already proceeded
                            //so new cart is needed
                            //create new cart id
                            Cart cart = new Cart
                            {
                                //save the new cart object to the table
                                AccountId = (int)accountId,
                                CreatedOn = DateTime.Now
                            };
                            db.Cart.Add(cart);
                            db.SaveChanges();

                            //get the new cart id
                            var newCart = db.Cart.Where(x => x.AccountId == accountId).OrderByDescending(x => x.CreatedOn).First();
                            item.CartId = newCart.CartId;

                            //to send to the view for the cartId
                            TempData["cartId"] = newCart.CartId;

                            //fill line item with cart info and save to db
                            //cartId of line item is already set in the if statement above
                            item.GameId = selectedGame.GameId;
                            item.Quantity = selectedQuantity;
                            item.Price = selectedGame.Price;
                            item.Option = option;
                            db.LineItem.Add(item);
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        //the user never add item to the Cart
                        //so needs to create new cart to hold the line item
                        //create new cart id
                        Cart cart = new Cart
                        {
                            //save the new cart object to the table
                            AccountId = (int)accountId,
                            CreatedOn = DateTime.Now
                        };
                        db.Cart.Add(cart);
                        db.SaveChanges();

                        //get the new cart id
                        var newCart = db.Cart.Where(x => x.AccountId == accountId).OrderByDescending(x => x.CreatedOn).First();
                        item.CartId = newCart.CartId;

                        //to send to the view for the cartId
                        TempData["cartId"] = newCart.CartId;

                        //fill line item with cart info and save to db
                        //cartId of line item is already set in the if statement above
                        item.GameId = selectedGame.GameId;
                        item.Quantity = selectedQuantity;
                        item.Price = selectedGame.Price;
                        db.LineItem.Add(item);
                        db.SaveChanges();
                    }
                }
                //show the cart list
                return RedirectToAction("ViewCart", "Cart");
            }
            else
            {
                TempData["message"] = "Need to login in order to add item to the cart";
                return RedirectToAction("Login", "Account");
            }
        }

        // POST: Cart/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Game game, int lineItemId)
        {
            int selectedQuantity = game.Quantity;

            var selectedGame = db.Game.SingleOrDefault(x => x.GameId == game.GameId);
            //1. if the game exist on the table
            if (selectedGame != null)
            {
                //2. update the quantity of the game
                //lineItemId = Request.Form["lineItemId"];
                //3. check the quantity of the lineitem
                var lineItem = db.LineItem.SingleOrDefault(x => x.LineItemId == lineItemId);

                //3. update the quantity of lineitem
                lineItem.Quantity = selectedQuantity;

                //4. update db
                db.Entry(lineItem).State = EntityState.Modified;

                //5. to send to the view for the cartId
                TempData["cartId"] = lineItem.CartId;

                db.SaveChanges();
            }
            return RedirectToAction("ViewCart");
        }

        // POST: Cart/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            //1. get the line item info to retrieve the game id
            var lineItem = db.LineItem.SingleOrDefault(x => x.LineItemId == id);

            //2. get the existing cart to delete it when all items are gone
            var existingCart = db.Cart.SingleOrDefault(x => x.CartId == lineItem.CartId);

            //3. delete the line item from line item db
            db.Entry(lineItem).State = EntityState.Modified;
            db.LineItem.Remove(lineItem);
            db.SaveChanges();

            //4. if there is no more line items with the existing cart id
            var lineItemList = db.LineItem.Where(x => x.CartId == lineItem.CartId).ToList();
            if (lineItemList.Count == 0)
            {
                //5. delete the cart from the cart table as well
                db.Entry(existingCart).State = EntityState.Modified;
                db.Cart.Remove(existingCart);
            }

            db.SaveChanges();

            //6. to send to the view for the cartId
            TempData["cartId"] = lineItem.CartId;

            return RedirectToAction("ViewCart");
        }

        // POST: Cart/Proceed
        [HttpPost, ActionName("Proceed")]
        [ValidateAntiForgeryToken]
        public ActionResult Proceed(int creditCardId, int addressId)
        {
            int accountId = Convert.ToInt32(Request.Form["accountId"]);

            if (accountId == 0)
            {
                TempData["message"] = "Need to login in order to checkout all items in the Cart";
                return RedirectToAction("Login", "Account");
            }
            else
            {

                var account = db.Account.Where(x => x.AccountId == accountId).SingleOrDefault();
                //1. save the credit card information to the lastUsedCreditCard on Account
                account.LastUsedCreditCard = creditCardId;
                //2. save the address information to the lastUsedAddressId on Account
                account.LastUsedAddressId = addressId;
                db.Entry(account).State = EntityState.Modified;
                db.SaveChanges();

                //3. create new order
                Order order = new Order
                {
                    OrderStatus = OrderStatus.InProcess,
                    OrderDate = DateTime.Now,
                    OrderTotal = Convert.ToDouble(Request.Form["orderTotal"]),
                    AccountId = accountId,
                    CreditCardId = creditCardId,
                    AddressId = addressId
                };

                //4. insert the new order into Order table
                db.Order.Add(order);
                db.SaveChanges();

                //5. get the latest Order id from the Order table with accountId
                var newOrder = db.Order.Where(x => x.AccountId == accountId).OrderByDescending(x => x.OrderDate).First();

                //6. find the line item list that has the same cart id
                int cartId = Convert.ToInt32(Request.Form["cartId"]);
                var lineItemList = db.LineItem.Where(x => x.CartId == cartId).ToList();

                //7. update the order id of the line item list
                if (lineItemList.Count != 0)
                {
                    foreach (var lineItem in lineItemList)
                    {
                        lineItem.OrderId = newOrder.OrderId;
                        //8. update LineItem DB with updated lineItemList that has the new order id
                        db.Entry(lineItem).State = EntityState.Modified;
                        db.SaveChanges();

                        //9. decrease the amount of stock as many as the number of checkout for all list items on Game table
                        var game = db.Game.SingleOrDefault(x => x.GameId == lineItem.GameId);
                        if(lineItem.Option != "Digital")
                        {
                            game.Quantity -= lineItem.Quantity;
                            db.Entry(game).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }

                return RedirectToAction("ViewOrder", "Order");
            }
        }
    }
}