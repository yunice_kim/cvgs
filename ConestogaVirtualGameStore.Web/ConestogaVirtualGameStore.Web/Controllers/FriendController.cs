﻿using ConestogaVirtualGameStore.Framework.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ConestogaVirtualGameStore.Web.Controllers
{
    [Authorize(Roles = "Customer")]
    public class FriendController : Controller
    {
        private readonly GameStoreContext db = new GameStoreContext();
        // GET: Friend
        public ActionResult Index()
        {
            
            int id = Convert.ToInt32(Session["accountId"]);
            CountLineItemInCart();
            return View(db.Friend.Where(x=> x.AccountId == id).ToList());
        }

        //public ActionResult Details(string userName)
        public ActionResult Details()
        {
            CountLineItemInCart();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SearchFriend(string userName)
        {
            var account = db.Account.Where(x => x.UserName == userName).FirstOrDefault();

           
            Account foundAccount = (Account)account;

            CountLineItemInCart();
            return View("FriendUser",account);
        }

        public ActionResult AddFriend(string userName)
        {
            //checks first if the logged in user is already friends with this user.
            int id = Convert.ToInt32(Session["accountId"]);
            Friend existingFriend = db.Friend.Where(x => x.userName == userName && x.AccountId == id).FirstOrDefault();
            if(existingFriend != null)
            {
                TempData["errorMessage"] = "You are already friends with this user";
                CountLineItemInCart();
                return RedirectToAction("Index");
            }

            Friend newFriend = new Friend
            {
                AccountId = id,
                userName = userName
            };
            db.Friend.Add(newFriend);
            db.SaveChanges();
            CountLineItemInCart();
            return RedirectToAction("Index");
        }

        

        public ActionResult GetFriendWishlist(string userName)
        {
            Account account = db.Account.Where(x => x.UserName == userName).FirstOrDefault();
            var wishlist = db.Wishlist.Where(x => x.AccountId == account.AccountId).SingleOrDefault();
            if(wishlist == null)
            {
                TempData["errorMessage"] = "User does not have a wishlist";
                CountLineItemInCart();
                return RedirectToAction("Index");
            }
            var wishlistItemlist = db.WishlistItem.Where(x => x.WishlistId == wishlist.WishlistId).ToList();

            TempData["Username"] = userName;
            CountLineItemInCart();
            return View("FriendWishlist", wishlistItemlist.ToList());
        }


        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Friend friend = db.Friend.Find(id);
            if (friend == null)
            {
                return HttpNotFound();
            }
           
            db.Friend.Remove(friend);
            db.SaveChanges();
            CountLineItemInCart();
            return RedirectToAction("Index");
        }

        public ActionResult GetFriendRequests()
        {
            int id = Convert.ToInt32(Session["accountId"]);

            
            Account account = db.Account.Where(x => x.AccountId == id).FirstOrDefault();

            // friends the logged in user is following
            List<Friend> existingFriends = db.Friend.Where(x => x.AccountId == id).ToList();
            // friends that are following the logged in user
            List<Friend> requestingFriends = db.Friend.Where(x => x.userName == account.UserName).ToList();
            List<Account> newFriends = new List<Account>();
            foreach (Friend friend in requestingFriends)
            {
                Account requestingFriendAccount = db.Account.Where(x => x.AccountId == friend.AccountId).FirstOrDefault();
                if (existingFriends.Count < 1)
                {
                    newFriends.Add(requestingFriendAccount);
                }
                else
                {
                    foreach (Friend existingFriend in existingFriends)
                    {
                        if (requestingFriendAccount.UserName != existingFriend.userName)
                        {
                            newFriends.Add(requestingFriendAccount);
                        }
                    }
                }
            }
            CountLineItemInCart();
            return View(newFriends);
        }

        public ActionResult RemoveRequest(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Friend friend = db.Friend.Where(x => x.AccountId == id).FirstOrDefault();
            if (friend == null)
            {
                return HttpNotFound();
            }

            db.Friend.Remove(friend);
            db.SaveChanges();
            CountLineItemInCart();
            return RedirectToAction("Index");
        }

        private void CountLineItemInCart()
        {
            var accountId = Convert.ToInt32(Session["accountId"]);
            var latestCart = db.Cart.Where(x => x.AccountId == accountId).OrderByDescending(x => x.CreatedOn).FirstOrDefault();
            if (latestCart != null)
            {
                var lineItemList = db.LineItem.Where(x => x.CartId == latestCart.CartId).ToList();

                //if the first line item's Order id is null, all list item for the cart id has all null for order id
                //and it means the cart isn't proceeded yet
                //so in order to show the count of line item list beside the Cart
                if (lineItemList[0].OrderId == null)
                {
                    TempData["lineItemCount"] = lineItemList.Count;
                }
                else
                {
                    TempData["lineItemCount"] = 0;
                }
            }
            else
            {
                TempData["lineItemCount"] = 0;
            }
        }
    }
}
