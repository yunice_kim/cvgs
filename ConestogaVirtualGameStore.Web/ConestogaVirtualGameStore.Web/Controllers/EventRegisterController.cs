﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ConestogaVirtualGameStore.Framework.Entities;

namespace ConestogaVirtualGameStore.Web.Controllers
{
    public class EventRegisterController : Controller
    {
        private GameStoreContext db = new GameStoreContext();
        // GET: EventRegister
        public ActionResult Index()
        {
            Session["eventRegisterId"] = null;
            var accountId = Convert.ToInt32(Session["accountId"]);
            var eventRegister = db.EventRegister.Include(e => e.Account).Where(e => e.AccountId == accountId).Include(e => e.Event);
            CountLineItemInCart();
            return View(eventRegister.ToList());
        }

        // GET: EventRegister/Details/5
        public ActionResult Details(int? id)
        {
            if (Session["eventRegisterId"] != null)
            {
                id = Convert.ToInt32(Session["eventRegisterId"]);
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EventRegister eventRegister = db.EventRegister.Find(id);
            if (eventRegister == null)
            {
                return HttpNotFound();
            }
            CountLineItemInCart();
            return View(eventRegister);
        }

        // GET: EventRegister/Create
        public ActionResult Create()
        {
            var accountId = Convert.ToInt32(Session["accountId"]);
            var eventId = Convert.ToInt32(Session["eventId"]);
            EventRegister eventRegister = db.EventRegister.Where(x => x.EventId == eventId).Where(x => x.AccountId == accountId).FirstOrDefault();

            if (eventRegister != null)
            {
                Session["eventRegisterId"] = eventRegister.EventRegisterId;
                return RedirectToAction("Details");
            }
            ViewBag.AccountId = new SelectList(db.Account, "AccountId", "UserName", accountId);
            ViewBag.EventId = new SelectList(db.Event, "EventId", "Title", eventId);
            CountLineItemInCart();
            return View();
        }

        // POST: EventRegister/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EventRegisterId,EventId,AccountId,DateRegistered")] EventRegister eventRegister)
        {
            
            int accountId = 0;
            int eventId = 0;
            if (eventRegister.AccountId != Convert.ToInt32(null) || eventRegister.AccountId != 0)
            {
                accountId = eventRegister.AccountId;
            }
            else if(Session["accountId"]?.ToString() != null)
            {
                accountId = Convert.ToInt32(Session["accountId"]);
            }
            if (eventRegister.EventId != Convert.ToInt32(null) || eventRegister.EventId != 0)
            {
                eventId = eventRegister.EventId;
            }
            else if (Session["eventId"]?.ToString() != null)
            {
                eventId = Convert.ToInt32(Session["eventId"]);
            }
            if (ModelState.IsValid)
                {
                     
                    if (accountId == 0)
                    {
                        TempData["message"] = "Need to login in order to register for event";
                        return RedirectToAction("Login", "Account");
                    }
                    EventRegister eventRegistered = db.EventRegister.Where(x => x.EventId == eventId).Where(x => x.AccountId == accountId).FirstOrDefault();
                    if (eventRegistered != null)
                    {
                        ViewBag.AccountId = new SelectList(db.Account, "AccountId", "UserName", accountId);
                        ViewBag.EventId = new SelectList(db.Event, "EventId", "Title", eventId);

                        TempData["msg"] = "You have already registered for this event.";
                        return RedirectToAction("Details", "Event", new { id = eventRegistered.EventId });
                    }
                    eventRegister.AccountId = accountId;
                    eventRegister.EventId = eventId;
                    eventRegister.DateRegistered = DateTime.Now;
                    db.EventRegister.Add(eventRegister);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.AccountId = new SelectList(db.Account, "AccountId", "UserName", eventRegister.AccountId);
                ViewBag.EventId = new SelectList(db.Event, "EventId", "Title", eventRegister.EventId);
                return View(eventRegister);
            
            
            
        }

        // GET: EventRegister/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EventRegister eventRegister = db.EventRegister.Find(id);
            if (eventRegister == null)
            {
                return HttpNotFound();
            }
            CountLineItemInCart();
            return View(eventRegister);
        }

        // POST: EventRegister/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EventRegister eventRegister = db.EventRegister.Find(id);
            db.EventRegister.Remove(eventRegister);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private void CountLineItemInCart()
        {
            var accountId = Convert.ToInt32(Session["accountId"]);
            var latestCart = db.Cart.Where(x => x.AccountId == accountId).OrderByDescending(x => x.CreatedOn).FirstOrDefault();
            if (latestCart != null)
            {
                var lineItemList = db.LineItem.Where(x => x.CartId == latestCart.CartId).ToList();

                //if the first line item's Order id is null, all list item for the cart id has all null for order id
                //and it means the cart isn't proceeded yet
                //so in order to show the count of line item list beside the Cart
                if (lineItemList[0].OrderId == null)
                {
                    TempData["lineItemCount"] = lineItemList.Count;
                }
                else
                {
                    TempData["lineItemCount"] = 0;
                }
            }
        }
    }
}
