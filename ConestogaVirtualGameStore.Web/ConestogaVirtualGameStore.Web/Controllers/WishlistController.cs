﻿using ConestogaVirtualGameStore.Framework.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConestogaVirtualGameStore.Web.Controllers
{
    public class WishlistController : Controller
    {
        private readonly GameStoreContext db = new GameStoreContext();

        // GET: Wishlist
        public ActionResult Index()
        {
            var accountId = Convert.ToInt32(Session["accountId"]);
            var latestCart = db.Cart.Where(x => x.AccountId == accountId).OrderByDescending(x => x.CreatedOn).FirstOrDefault();

            if (TempData["logout"] != null && TempData["logout"].ToString() == "logout")
            {//when logout, just show the game list
                return View(db.WishlistItem.ToList());
            }
            else if (TempData["logout"] == null && latestCart == null)
            {//it's not logout, but need to show the count of line item on the Cart when customer never added item to the Cart
                TempData["lineItemCount"] = 0;
                return View(db.WishlistItem.ToList());
            }
            else if (accountId == 0)
            {//session expired
                TempData["message"] = "Session expired. Login please";
                return RedirectToAction("Login", "Account");
            }
            else
            {//show the count of line item on the Cart
                if (latestCart != null)
                {
                    CountLineItemInCart();
                }

                //to get the wishlist item list
                var wishlist = db.Wishlist.Where(x => x.AccountId == accountId).SingleOrDefault();
                
                if (wishlist != null)
                {
                    var wishlistItemlist = db.WishlistItem.Where(x => x.WishlistId == wishlist.WishlistId).ToList();
                    if(wishlistItemlist.Count == 0)
                    {
                        TempData["wishlistCount"] = "0";
                    }
                    CountLineItemInCart();
                    return View(wishlistItemlist.ToList());
                }
                else
                {
                    TempData["wishlistCount"] = "0";
                    CountLineItemInCart();
                    return View();
                }
            }
        }

        private void CountLineItemInCart()
        {
            var accountId = Convert.ToInt32(Session["accountId"]);
            var latestCart = db.Cart.Where(x => x.AccountId == accountId).OrderByDescending(x => x.CreatedOn).FirstOrDefault();
            if (latestCart != null)
            {
                var lineItemList = db.LineItem.Where(x => x.CartId == latestCart.CartId).ToList();

                //if the first line item's Order id is null, all list item for the cart id has all null for order id
                //and it means the cart isn't proceeded yet
                //so in order to show the count of line item list beside the Cart
                if (lineItemList[0].OrderId == null)
                {
                    TempData["lineItemCount"] = lineItemList.Count;
                }
                else
                {
                    TempData["lineItemCount"] = 0;
                }
            }
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult AddToWishlist(int gameId, int accountId, string keyword)
        {
            //1. check if the user already created Wishlist before
            Wishlist wishlist = db.Wishlist.Where(x => x.AccountId == accountId).FirstOrDefault();

            //2. if the user doesn't have Wishlist, create a Wishlist for the user
            if (wishlist == null)
            {
                wishlist = new Wishlist
                {
                    AccountId = accountId,
                    CreatedDate = DateTime.Now
                };

                db.Wishlist.Add(wishlist);
                db.SaveChanges();

                //to get the wishlist id
                wishlist = db.Wishlist.Where(x => x.AccountId == accountId).FirstOrDefault();
            }

            //3. check if the game is already in the Wishlist
            var existedWishlistItem = db.WishlistItem.Where(x => x.WishlistId == wishlist.WishlistId && x.GameId == gameId).SingleOrDefault();

            //4. if the game is not in the Wishlist, add it to the Wishlist

            if(existedWishlistItem == null)
            {
                //add the gameId into WishlistItem
                WishlistItem wishlistItem = new WishlistItem
                {
                    GameId = gameId,
                    WishlistId = wishlist.WishlistId
                };

                db.WishlistItem.Add(wishlistItem);
                db.SaveChanges();

                TempData["WishlistMessage"] = "The game is successfully added to Wishlist";
            }
            else
            {
                ViewBag.keyword = keyword;
                TempData["WishlistMessage"] = "The game already exists in Wishlist";
            }
            ViewBag.keyword = keyword;
            if (!string.IsNullOrEmpty(keyword)){
                return RedirectToAction("SearchGameWithKeyword", "Game", new { keyword });
            }
            else {
                return RedirectToAction("Index", "Game", new { keyword }); 
            }
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int wishlistItemId, int accountId)
        {
            
            //1. get the wishlist item info to retrieve the wishlist id
            var wishlistItem = db.WishlistItem.SingleOrDefault(x => x.WishlistItemId == wishlistItemId);

            //2. get the existing wishlist to delete it when the owner is the user
            var existingWishlist = db.Wishlist.SingleOrDefault(x => x.WishlistId == wishlistItem.WishlistId);

            if(accountId != null)
            {
                if(accountId == existingWishlist.AccountId)
                {
                    //3. delete the line item from line item db
                    db.Entry(wishlistItem).State = EntityState.Modified;
                    db.WishlistItem.Remove(wishlistItem);
                    db.SaveChanges();
                    TempData["wishlistMessage"] = "Deleted successfully";

                }
                else
                {
                    TempData["wishlistMessage"] = "The item on the Wishlist is not added by this user. Can't remove it from this Wishlist";

                }
            }

            //to get the wishlist item list
            var wishlist = db.Wishlist.Where(x => x.AccountId == accountId).SingleOrDefault();

            if (wishlist == null)
            {
                TempData["wishlistCount"] = "0";
            }
            
            //6. to send to the view for the cartId
            //TempData["cartId"] = lineItem.CartId;

            return RedirectToAction("Index");
        }
    }
}