﻿using ConestogaVirtualGameStore.Framework.Entities;
using System;
using System.Data;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using ConestogaVirtualGameStore.Web.Models;

namespace ConestogaVirtualGameStore.Web.Controllers
{
    public class ProfileController : Controller
    {
        private readonly GameStoreContext db = new GameStoreContext();
        
        // GET: Profile
        public ActionResult Index()
        {
            
            var accountId = Convert.ToInt32(Session["accountId"]);

            if (accountId == 0)
            {
                TempData["message"] = "Session expired. Login please";
                return RedirectToAction("Login", "Account");
            }
            else
            {
                //1. get latest cart for the current account
                var latestCart = db.Cart.Where(x => x.AccountId == accountId).OrderByDescending(x => x.CreatedOn).FirstOrDefault();
                TempData["AccountState"] = db.Account.Where(x => x.AccountId == accountId).Select(a => a.AccountState).FirstOrDefault();
                if (latestCart != null)
                {
                    var lineItemList = db.LineItem.Where(x => x.CartId == latestCart.CartId).ToList();

                    //2. if the first line item's Order id is null, all list item for the cart id has all null for order id
                    //and it means the cart isn't proceeded yet
                    //so in order to show the count of line item list beside the Cart
                    if (lineItemList[0].OrderId == null)
                    {
                        TempData["lineItemCount"] = lineItemList.Count;
                    }
                    else
                    {
                        TempData["lineItemCount"] = 0;
                    }
                }
                else
                {
                    TempData["lineItemCount"] = 0;
                }

                return View();
            }
        }

        // GET: Preferences/Create
        public ActionResult CreatePreference()
        {
            var accountId = Convert.ToInt32(Session["accountId"]);

            if (accountId == 0)
            {
                TempData["message"] = "Session expired. Login please";
                return RedirectToAction("Login", "Account");
            }
            else
            {
                //1. get latest cart for the current account
                CountLineItemInCart();             
            }
            ViewBag.AccountId = new SelectList(db.Account, "AccountId", "UserName", accountId);
            ViewBag.GameCategoryId = new SelectList(db.GameCategory, "GameCategoryId", "CategoryName");
            ViewBag.GamePlatformCode = new SelectList(db.GamePlatform, "GamePlatformCode", "Name");
            return View("CreatePreference");
        }

        private void CountLineItemInCart()
        {
            var accountId = Convert.ToInt32(Session["accountId"]);
            var latestCart = db.Cart.Where(x => x.AccountId == accountId).OrderByDescending(x => x.CreatedOn).FirstOrDefault();
            if (latestCart != null)
            {
                var lineItemList = db.LineItem.Where(x => x.CartId == latestCart.CartId).ToList();

                //if the first line item's Order id is null, all list item for the cart id has all null for order id
                //and it means the cart isn't proceeded yet
                //so in order to show the count of line item list beside the Cart
                if (lineItemList[0].OrderId == null)
                {
                    TempData["lineItemCount"] = lineItemList.Count;
                }
                else
                {
                    TempData["lineItemCount"] = 0;
                }
            }
        }

        // POST: Preferences/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePreference([Bind(Include = "PreferenceId,GameCategoryId,GamePlatformCode,AccountId")] Preference preference)
        {
            
            if (ModelState.IsValid)
            {
                db.Preferences.Add(preference);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AccountId = new SelectList(db.Account, "AccountId", "UserName", preference.AccountId);
            ViewBag.GameCategoryId = new SelectList(db.GameCategory, "GameCategoryId", "CategoryName", preference.GameCategoryId);
            ViewBag.GamePlatformCode = new SelectList(db.GamePlatform, "GamePlatformCode", "Name", preference.GamePlatformCode);
            return View(preference);
        }

        public ActionResult EditPreferences()
        {
            var accountId = Convert.ToInt32(Session["accountId"]);

            if (accountId == 0)
            {
                TempData["message"] = "Session expired. Login please";
                return RedirectToAction("Login", "Account");
            }
            else
            {
                //1. get latest cart for the current account
                var latestCart = db.Cart.Where(x => x.AccountId == accountId).OrderByDescending(x => x.CreatedOn).FirstOrDefault();
                if (latestCart != null)
                {
                    var lineItemList = db.LineItem.Where(x => x.CartId == latestCart.CartId).ToList();

                    //2. if the first line item's Order id is null, all list item for the cart id has all null for order id
                    //and it means the cart isn't proceeded yet
                    //so in order to show the count of line item list beside the Cart
                    if (lineItemList[0].OrderId == null)
                    {
                        TempData["lineItemCount"] = lineItemList.Count;
                    }
                    else
                    {
                        TempData["lineItemCount"] = 0;
                    }
                }
                else
                {
                    TempData["lineItemCount"] = 0;
                }
            }
            Preference preference = db.Preferences.Where(x => x.AccountId == accountId).FirstOrDefault();
            if (preference == null)
            {
                return RedirectToAction("CreatePreference");
            }
            ViewBag.AccountId = new SelectList(db.Account, "AccountId", "UserName", accountId);
            ViewBag.GameCategoryId = new SelectList(db.GameCategory, "GameCategoryId", "CategoryName", preference.GameCategoryId);
            ViewBag.GamePlatformCode = new SelectList(db.GamePlatform, "GamePlatformCode", "Name", preference.GamePlatformCode);
            return View(preference);
        }

        // POST: Preferences/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPreferences([Bind(Include = "PreferenceId,GameCategoryId,GamePlatformCode,AccountId")] Preference preference)
        {
            if (ModelState.IsValid)
            {
                db.Entry(preference).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AccountId = new SelectList(db.Account, "AccountId", "UserName", preference.AccountId);
            ViewBag.GameCategoryId = new SelectList(db.GameCategory, "GameCategoryId", "CategoryName", preference.GameCategoryId);
            ViewBag.GamePlatformCode = new SelectList(db.GamePlatform, "GamePlatformCode", "Name", preference.GamePlatformCode);
            return View(preference);
        }

        //HttpGet
        public ActionResult ChangePassword()
        {
            var accountId = Convert.ToInt32(Session["accountId"]);

            if (accountId == 0)
            {
                TempData["message"] = "Session expired. Login please";
                
                return RedirectToAction("Login", "Account");
            }
            else
            {
                ChangePasswordViewModel changePassword = new ChangePasswordViewModel();
                TempData["passwordChange"] = null;
                CountLineItemInCart();
                return View(changePassword);
            }
            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangePasswordViewModel changePassword)
        {
            var accountId = Convert.ToInt32(Session["accountId"]);
            if (accountId == 0)
            {
                TempData["message"] = "Session expired. Login please";

                return RedirectToAction("Login", "Account");
            }
            else
            {
                Account account = db.Account.Where(x => x.AccountId == accountId).FirstOrDefault();
                if (ModelState.IsValid)
                {
                    if(changePassword.OldPassword == account.Password)
                    {
                        if (changePassword.NewPassword == changePassword.ConfirmPassword)
                        {
                            account.Password = changePassword.NewPassword;
                            db.Entry(account).State = EntityState.Modified;
                            db.SaveChanges();
                            TempData["passwordChange"] = "Password changed successfully!";
                            CountLineItemInCart();
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            TempData["passwordChange"] = "Confirmed passwords must match";
                        }
                    }
                    else
                    {
                        TempData["passwordChange"] = "Current password incorrect";
                    }
                    

                }
                ChangePasswordViewModel changePasswordVM = new ChangePasswordViewModel();
                CountLineItemInCart();
                return View(changePasswordVM);
            }
            
        }
    }
}