﻿using ConestogaVirtualGameStore.Framework.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConestogaVirtualGameStore.Web.Controllers
{
    [Authorize(Roles = "Customer")]
    public class OrderController : Controller
    {
        private readonly GameStoreContext db = new GameStoreContext();
        
        // GET: Order/ViewOrder
        public ActionResult ViewOrder()
        {
            var accountId = Convert.ToInt32(Session["accountId"]);

            if (accountId == 0)
            {
                TempData["message"] = "Need to login in order to see the Order";
                return RedirectToAction("Login", "Account");
            }
            else
            {
                //1. get order list from Order table with accountId
                var orderList = db.Order.Where(x => x.AccountId == accountId).ToList();

                //TempData["message"] = null;
                //2. get latest cart for the current account
                CountLineItemInCart();

                //4. return to the OrderView with the order list
                return View(orderList.ToList());
            }
        }

        private void CountLineItemInCart()
        {
            var accountId = Convert.ToInt32(Session["accountId"]);
            var latestCart = db.Cart.Where(x => x.AccountId == accountId).OrderByDescending(x => x.CreatedOn).FirstOrDefault();
            if (latestCart != null)
            {
                var lineItemList = db.LineItem.Where(x => x.CartId == latestCart.CartId).ToList();

                //if the first line item's Order id is null, all list item for the cart id has all null for order id
                //and it means the cart isn't proceeded yet
                //so in order to show the count of line item list beside the Cart
                if (lineItemList[0].OrderId == null)
                {
                    TempData["lineItemCount"] = lineItemList.Count;
                }
                else
                {
                    TempData["lineItemCount"] = 0;
                }
            }
        }

        // GET: Order/Details/5
        public ActionResult Details(int? orderId, int? accountId)
        {
            //accountId = Convert.ToInt32(Session["accountId"]);

            if (accountId == null)
            {
                TempData["message"] = "Need to login in order to see the details of Order";
                return RedirectToAction("Login", "Account");
            }
            else
            {
                //1. get the total of current order
                var order = db.Order.Find(orderId);
                ViewBag.Order= order;
                //TempData["total"] = order.OrderTotal;

                //2. retrieve the line item list with the order Id that are already ordered
                var lineItemList = db.LineItem.Where(x => x.OrderId == orderId).ToList();

                //3. get the used checkout information for billing address and credit card 
                var address = db.Address.Where(x => x.AddressId == order.AddressId).SingleOrDefault();
                var creditCard = db.CreditCard.Where(x => x.CreditCardId == order.CreditCardId).SingleOrDefault();
                //3.1 save address and credit card information to the ViewBag to send them to the Details view
                ViewBag.Address = address;
                ViewBag.CreditCard = creditCard;


                //4. check if any items in the cart to show the count beside Cart
                CountLineItemInCart();
                
                //5. return the lineItemList to the Details view
                return View(lineItemList.ToList());
            }
        }

        public ActionResult DownloadGame(string gameName)
        {
            TempData["orderMessage"] = "Downloaded " + gameName;
            return RedirectToAction("ViewOrder");
        }
    }
}