﻿using ConestogaVirtualGameStore.Framework.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ConestogaVirtualGameStore.Web.Controllers
{
    public class EventController : Controller
    {
        private GameStoreContext db = new GameStoreContext();
        // GET: Event
        public ActionResult Index()
        {
            var accountId = Convert.ToInt32(Session["accountId"]);
            var latestCart = db.Cart.Where(x => x.AccountId == accountId).OrderByDescending(x => x.CreatedOn).FirstOrDefault();

            if (TempData["logout"] != null && TempData["logout"].ToString() == "logout")
            {
                return View(db.Event.ToList());
            }

            else if (TempData["logout"] == null && latestCart == null)
            {
                TempData["lineItemCount"] = 0;
                return View(db.Event.ToList());
            }
            else if (accountId == 0)
            {
                TempData["message"] = "Session expired. Login please";
                return RedirectToAction("Login", "Account");
            }
            else
            {
                if (latestCart != null)
                {
                    CountLineItemInCart();
                }

                return View(db.Event.ToList());
            }           

        }

        private void CountLineItemInCart()
        {
            var accountId = Convert.ToInt32(Session["accountId"]);
            var latestCart = db.Cart.Where(x => x.AccountId == accountId).OrderByDescending(x => x.CreatedOn).FirstOrDefault();
            if (latestCart != null)
            {
                var lineItemList = db.LineItem.Where(x => x.CartId == latestCart.CartId).ToList();

                //if the first line item's Order id is null, all list item for the cart id has all null for order id
                //and it means the cart isn't proceeded yet
                //so in order to show the count of line item list beside the Cart
                if (lineItemList[0].OrderId == null)
                {
                    TempData["lineItemCount"] = lineItemList.Count;
                }
                else
                {
                    TempData["lineItemCount"] = 0;
                }
            }
        }

        // GET: Event/Details/5
        public ActionResult Details(int? id)
        {
            Session["eventId"] = id;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event eventSelected = db.Event.Find(id);
            if (eventSelected == null)
            {
                return HttpNotFound();
            }
            TempData["eventName"] = eventSelected.Title;


            //1. get latest cart for the current account
            var accountId = Convert.ToInt32(Session["accountId"]);
            var latestCart = db.Cart.Where(x => x.AccountId == accountId).OrderByDescending(x => x.CreatedOn).FirstOrDefault();
            if (latestCart != null)
            {
                var lineItemList = db.LineItem.Where(x => x.CartId == latestCart.CartId).ToList();

                //2. if the first line item's Order id is null, all list item for the cart id has all null for order id
                //and it means the cart isn't proceeded yet
                //so in order to show the count of line item list beside the Cart
                if (lineItemList[0].OrderId == null)
                {
                    TempData["lineItemCount"] = lineItemList.Count;
                }
                else
                {
                    TempData["lineItemCount"] = 0;
                }
            }
            else
            {
                TempData["lineItemCount"] = 0;
            }

            return View("Details", eventSelected);
        }
    }
}