﻿using ConestogaVirtualGameStore.Framework.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ConestogaVirtualGameStore.Web.Controllers
{
    public class GameController : Controller
    {
        private readonly GameStoreContext db = new GameStoreContext();
        // GET: Game
        public ActionResult Index()
        {
            var accountId = Convert.ToInt32(Session["accountId"]);
            if(accountId != 0)
            {
                var person = db.Account.SingleOrDefault(x => x.AccountId == accountId);
                var employee = db.Employee.SingleOrDefault(x => x.PersonId == person.PersonId);
                // if the account id is for employee, make the employee logout and show the login screen
                if (employee != null)
                {
                    //ModelState.AddModelError("UserName", "Entered UserName is for Customer. Try it on Customer's site");
                    TempData["message"] = "You cannot access the Customer site with the Employee user name";
                    FormsAuthentication.SignOut();
                    Session.Clear();
                    return RedirectToAction("Login", "Account");
                }
            }

            var latestCart = db.Cart.Where(x => x.AccountId == accountId).OrderByDescending(x => x.CreatedOn).FirstOrDefault();
            
            if (TempData["logout"] != null && TempData["logout"].ToString() == "logout")
            {//when logout, just show the game list
                return View(db.Game.ToList());
            }
            else if(TempData["logout"] == null && latestCart == null)
            {//it's not logout, but need to show the count of line item on the Cart when customer never added item to the Cart
                TempData["lineItemCount"] = 0;
                return View(db.Game.ToList());
            }
            else if (accountId == 0)
            {//session expired
                TempData["message"] = "Session expired. Login please";
                return RedirectToAction("Login", "Account");
            }
            else
            {//show the count of line item on the Cart
                if (latestCart != null)
                {
                    var lineItemList = db.LineItem.Where(x => x.CartId == latestCart.CartId).ToList();

                    //if the first line item's Order id is null, all list item for the cart id has all null for order id
                    //and it means the cart isn't proceeded yet
                    //so in order to show the count of line item list beside the Cart
                    if (lineItemList[0].OrderId == null)
                    {
                        TempData["lineItemCount"] = lineItemList.Count;
                    }
                    else
                    {
                        TempData["lineItemCount"] = 0;
                    }
                }

                return View(db.Game.ToList());
            }
        }

        // GET: Review/Create
        public ActionResult CreateReview(string gameName, int gameId)
        {
            TempData["gameName"] = gameName;
            TempData["gameId"] = gameId;

            CountLineItemInCart();
            //ViewBag.GameId = new SelectList(db.Game, "GameId", "Name");
            return View();
        }

        // POST: Review/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateReview([Bind(Include = "ReviewId,Ratings,Details,GameId")] Review review)
        {
            if (ModelState.IsValid)
            {
                db.Review.Add(review);
                db.SaveChanges();
                //return Redirect("/Game/Details/" + review.GameId);
                CountLineItemInCart();
                TempData["reviewMessage"] = "Thank you for your feedback! Your review will be added once approved by administrator.";
                return RedirectToAction("Details", new { id = review.GameId });
            }

            ViewBag.GameId = new SelectList(db.Game, "GameId", "Name", review.GameId);
            return View(review);
        }

        // GET: Game/Details/5
        public ActionResult Details(int? id)
        {
            TempData["averageRating"] = null;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Game game = db.Game.Find(id);
            double averageRating = CalculateAverageRating(id);
            TempData["averageRating"] = averageRating;
            if (game == null)
            {
                return HttpNotFound();
            }
            TempData["gameName"] = game.Name;

            ////1. get latest cart for the current account
            //var accountId = Convert.ToInt32(Session["accountId"]);
            CountLineItemInCart(game.GameId);
            IEnumerable<Review> reviews = new List<Review>();
            reviews = db.Review.Where(x => x.GameId == id && x.Approved).ToList();

            ViewData["reviews"] = reviews;
            return View(game);
        }

        private void CountLineItemInCart(int gameId)
        {
            //1. get latest cart for the current account
            var accountId = Convert.ToInt32(Session["accountId"]);
            var latestCart = db.Cart.Where(x => x.AccountId == accountId).OrderByDescending(x => x.CreatedOn).FirstOrDefault();
            if (latestCart != null)
            {
                var lineItemList = db.LineItem.Where(x => x.CartId == latestCart.CartId).ToList();

                //2. if the first line item's Order id is null, all list item for the cart id has all null for order id
                //and it means the cart isn't proceeded yet
                //so in order to show the count of line item list beside the Cart
                if (lineItemList[0].OrderId == null)
                {
                    TempData["lineItemCount"] = lineItemList.Count;
                    foreach (var lineItem in lineItemList)
                    {
                        if (lineItem.GameId == gameId)
                        {
                            TempData["selectedQuantity"] = lineItem.Quantity;
                            break;
                        }
                    }
                }
                else
                {
                    TempData["lineItemCount"] = 0;
                }
            }
            else
            {
                TempData["lineItemCount"] = 0;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SearchGame(string keyword)
        {
            if (ModelState.IsValid)
            {
                var gameResultList = db.Game.Where(x => x.Name.Contains(keyword)).ToList();
                ViewBag.keyword = keyword;
                //db.SaveChanges();

                CountLineItemInCart();              
                return View("GameResultList", gameResultList);
            }
            return View();
        }

        private void CountLineItemInCart()
        {
            var accountId = Convert.ToInt32(Session["accountId"]);
            var latestCart = db.Cart.Where(x => x.AccountId == accountId).OrderByDescending(x => x.CreatedOn).FirstOrDefault();
            if (latestCart != null)
            {
                var lineItemList = db.LineItem.Where(x => x.CartId == latestCart.CartId).ToList();

                //if the first line item's Order id is null, all list item for the cart id has all null for order id
                //and it means the cart isn't proceeded yet
                //so in order to show the count of line item list beside the Cart
                if (lineItemList[0].OrderId == null)
                {
                    TempData["lineItemCount"] = lineItemList.Count;
                }
                else
                {
                    TempData["lineItemCount"] = 0;
                }
            }
        }
        public ActionResult SearchGameWithKeyword(string keyword)
        {
            //string keyword = ViewBag.keyword;
            var gameResultList = db.Game.Where(x => x.Name.Contains(keyword)).ToList();

            ViewBag.keyword = keyword;
            CountLineItemInCart();
            //db.SaveChanges();
            //return Redirect("/Game/Details/" + review.GameId);
            return View("GameResultList", gameResultList);
        }

        public double CalculateAverageRating(int? gameId)
        {
            if(db.Review.Where(a => a.GameId == gameId && a.Approved == true).Count() == 0)
            {
                return 0;
            }
            int star5 = db.Review.Where(a => a.GameId == gameId && a.Ratings == 5 && a.Approved == true).Count();
            int star4 = db.Review.Where(a => a.GameId == gameId && a.Ratings == 4 && a.Approved == true).Count();
            int star3 = db.Review.Where(a => a.GameId == gameId && a.Ratings == 3 && a.Approved == true).Count();
            int star2 = db.Review.Where(a => a.GameId == gameId && a.Ratings == 2 && a.Approved == true).Count();
            int star1 = db.Review.Where(a => a.GameId == gameId && a.Ratings == 1 && a.Approved == true).Count();

            double averageRating = 0;

            averageRating = (double)(5 * star5 + 4 * star4 + 3 * star3 + 2 * star2 + 1 * star1) / (star1 + star2 + star3 + star4 + star5);

            averageRating = Math.Round(averageRating, 1);

            return averageRating;
        }
    }
}