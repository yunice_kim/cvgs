﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ConestogaVirtualGameStore.Framework.Entities;

namespace ConestogaVirtualGameStore.Web.Controllers
{
    [Authorize(Roles = "Customer")]
    public class CreditCardController : Controller
    {
        private readonly GameStoreContext db = new GameStoreContext();

        // GET: CreditCard
        public ActionResult Index()
        {
            int accountId = Convert.ToInt32(Session["accountId"]);

            if (accountId == 0)
            {
                TempData["message"] = "Need to login in order to see credit card information";
                return RedirectToAction("Login", "Account");
            }
            else
            {
                //1. in order to get the count of items on the cart
                CountLineItemInCart();
                
                //3. get credit card list for the account
                var creditCardList = db.CreditCard.Where(x => x.AccountId == accountId).ToList();

                return View(creditCardList.ToList());
            }
        }

        private void CountLineItemInCart()
        {
            var accountId = Convert.ToInt32(Session["accountId"]);
            var latestCart = db.Cart.Where(x => x.AccountId == accountId).OrderByDescending(x => x.CreatedOn).FirstOrDefault();
            if (latestCart != null)
            {
                var lineItemList = db.LineItem.Where(x => x.CartId == latestCart.CartId).ToList();

                //if the first line item's Order id is null, all list item for the cart id has all null for order id
                //and it means the cart isn't proceeded yet
                //so in order to show the count of line item list beside the Cart
                if (lineItemList[0].OrderId == null)
                {
                    TempData["lineItemCount"] = lineItemList.Count;
                }
                else
                {
                    TempData["lineItemCount"] = 0;
                }
            }
        }

        // GET: CreditCard/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CreditCard creditCard = db.CreditCard.Find(id);
            if (creditCard == null)
            {
                return HttpNotFound();
            }
            int accountId = Convert.ToInt32(Session["accountId"]);

            if (accountId == 0)
            {
                TempData["message"] = "Need to login in order to see credit card information";
                return RedirectToAction("Login", "Account");
            }
            else
            {
                //1. in order to get the count of items on the cart
                CountLineItemInCart();
            }

            return View(creditCard);
        }

        // GET: CreditCard/Create
        public ActionResult Create()
        {
            //ViewBag.AccountId = new SelectList(db.Account, "AccountId", "UserName");
            int accountId = Convert.ToInt32(Session["accountId"]);

            if (accountId == 0)
            {
                TempData["message"] = "Need to login in order to see credit card information";
                return RedirectToAction("Login", "Account");
            }
            else
            {
                //1. in order to get the count of items on the cart
                CountLineItemInCart();
                return View();
            }
        }

        // POST: CreditCard/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CreditCardId,Cardholder,CardNumber,CVV,ExpiryDate,AccountId")] CreditCard creditCard , int? accountId)
        {
            if (accountId == 0)
            {
                TempData["message"] = "Need to login in order to add credit card information.";
                return RedirectToAction("Login", "Account");
            }
            else
            {
                if (ModelState.IsValid)
                {
                    var creditCard_db = db.CreditCard.SingleOrDefault(x => x.CardNumber == creditCard.CardNumber);
                    if (creditCard_db != null)
                    {
                        ModelState.AddModelError("CardNumber", "Credit card is already registered.");
                        TempData["message"] = "Credit card is already registered. Please try a different one.";
                        return View("Create");
                    }
                    else
                    {
                        creditCard.Cardholder = creditCard.Cardholder.ToUpper();
                        creditCard.AccountId = (int)accountId;
                        db.CreditCard.Add(creditCard);
                        db.SaveChanges();
                    }
              
                }

                //1. in order to get the count of items on the cart
                var latestCart = db.Cart.Where(x => x.AccountId == accountId).OrderByDescending(x => x.CreatedOn).FirstOrDefault();
                if (latestCart != null)
                {
                    var lineItemList = db.LineItem.Where(x => x.CartId == latestCart.CartId).ToList();
                    //2. if the first line item's Order id is null, all list item for the cart id has all null for order id
                    //and it means the cart isn't proceeded yet
                    if (lineItemList[0].OrderId == null)
                    {
                        TempData["lineItemCount"] = lineItemList.Count;
                    }
                }

                return RedirectToAction("Index", creditCard);
            }

            //ViewBag.AccountId = new SelectList(db.Account, "AccountId", "UserName", creditCard.AccountId);
            
        }

        // GET: CreditCard/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CreditCard creditCard = db.CreditCard.Find(id);
            if (creditCard == null)
            {
                return HttpNotFound();
            }
            ViewBag.AccountId = new SelectList(db.Account, "AccountId", "UserName", creditCard.AccountId);

            //1. in order to get the count of items on the cart
            CountLineItemInCart();

            return View(creditCard);
        }

        // POST: CreditCard/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CreditCardId,Cardholder,CardNumber,CVV,ExpiryDate,AccountId")] CreditCard creditCard, int? accountId)
        {
            if (ModelState.IsValid)
            {
                db.Entry(creditCard).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AccountId = new SelectList(db.Account, "AccountId", "UserName", creditCard.AccountId);

            //1. in order to get the count of items on the cart
            var latestCart = db.Cart.Where(x => x.AccountId == creditCard.AccountId).OrderByDescending(x => x.CreatedOn).FirstOrDefault();
            if (latestCart != null)
            {
                var lineItemList = db.LineItem.Where(x => x.CartId == latestCart.CartId).ToList();
                //2. if the first line item's Order id is null, all list item for the cart id has all null for order id
                //and it means the cart isn't proceeded yet
                if (lineItemList[0].OrderId == null)
                {
                    TempData["lineItemCount"] = lineItemList.Count;

                }
            }
            return View(creditCard);
        }

        // GET: CreditCard/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CreditCard creditCard = db.CreditCard.Find(id);
            if (creditCard == null)
            {
                return HttpNotFound();
            }
            //1. in order to get the count of items on the cart
            var latestCart = db.Cart.Where(x => x.AccountId == creditCard.AccountId).OrderByDescending(x => x.CreatedOn).FirstOrDefault();
            if (latestCart != null)
            {
                var lineItemList = db.LineItem.Where(x => x.CartId == latestCart.CartId).ToList();
                //2. if the first line item's Order id is null, all list item for the cart id has all null for order id
                //and it means the cart isn't proceeded yet
                if (lineItemList[0].OrderId == null)
                {
                    TempData["lineItemCount"] = lineItemList.Count;

                }
            }

            return View(creditCard);
        }

        // POST: CreditCard/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CreditCard creditCard = db.CreditCard.Find(id);
            db.CreditCard.Remove(creditCard);
            db.SaveChanges();

            //1. in order to get the count of items on the cart
            var latestCart = db.Cart.Where(x => x.AccountId == creditCard.AccountId).OrderByDescending(x => x.CreatedOn).FirstOrDefault();
            if (latestCart != null)
            {
                var lineItemList = db.LineItem.Where(x => x.CartId == latestCart.CartId).ToList();
                //2. if the first line item's Order id is null, all list item for the cart id has all null for order id
                //and it means the cart isn't proceeded yet
                if (lineItemList[0].OrderId == null)
                {
                    TempData["lineItemCount"] = lineItemList.Count;

                }
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
