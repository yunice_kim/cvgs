﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ConestogaVirtualGameStore.Framework.Entities;
using System.Net.Mail;
using System.Configuration;
using ConestogaVirtualGameStore.Web.Models;
using Newtonsoft.Json;

namespace ConestogaVirtualGameStore.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly GameStoreContext db = new GameStoreContext();

        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            if (Session["accountId"] != null)
            {
                return RedirectToAction("Index", "Game");
            }
            else if(TempData["attempts"] != null && (int)TempData["attempts"] != 0)
            {
                return View("ForgotPassword");
            }

            return View("Login");
        }

        public ActionResult Signup()
        {
            return View();
        }
        // GET: Account/Details/5
        [Authorize(Roles = "Customer")]
        public ActionResult Details(int? id)
        {
            id = Convert.ToInt32(Session["accountId"]);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Account account = db.Account.Find(id);
            //int personId = Convert.ToInt32(Session["personId"]);
            //Person person = db.Person.Find(personId);
            if (account == null)
            {
                return HttpNotFound();
            }
            return View(account);
        }

        // GET: Account/Create
        public ActionResult Create()
        {
            ViewBag.PersonId = new SelectList(db.Person, "PersonId", "FirstName");
            return View();
        }

        // POST: Account/Login
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Account account)
        {
            if (ModelState.IsValid)
            {
                var user = db.Account.SingleOrDefault(x => x.UserName == account.UserName);

                if (user == null)
                {
                    ModelState.AddModelError("UserName", "Entered UserName doesn't exist");
                    return View("Login");
                }
                else
                {
                    var customer = db.Customer.SingleOrDefault(x => x.PersonId == user.PersonId);
                    if( customer == null)
                    {
                        ModelState.AddModelError("UserName", "Entered UserName is for Employee. Try it on Employee's site");
                        TempData["message"] = "You cannot login with the employee user name on this page";
                        return View("Login");
                    }
                    else
                    {
                        //locking check
                        if (user.Attempts == 3)
                        {
                            TempData["message"] = "This account is locked by 3 times wrong password input";
                            TempData["attempts"] = user.Attempts;
                            return RedirectToAction("Login", "Account");
                        }
                        else if (user.Password == account.Password)
                        {
                            if (user.AccountState == 0)
                            {
                                TempData["message"] = "Please validate your account through the activation link sent to your registered email.";
                                return View("Login");
                            }
                            else
                            {
                                TempData["message"] = user.UserName;
                                TempData["accountId"] = user.AccountId;
                                Session["accountId"] = user.AccountId.ToString();
                                Session["userName"] = user.UserName;
                                Session["personId"] = user.PersonId;
                                TempData["AccountState"] = user.AccountState;
                                //reset Attempts for login to 0
                                user.Attempts = 0;
                                db.Entry(user).State = EntityState.Modified;
                                db.SaveChanges();

                                //find the total of line item from db and send it to the view to let the cart have the count of line item
                                var latestCart = db.Cart.Where(x => x.AccountId == user.AccountId).OrderByDescending(x => x.CreatedOn).FirstOrDefault();

                                if (latestCart != null)
                                {
                                    var lineItemListInCart = db.LineItem.Where(x => x.CartId == latestCart.CartId).ToList();

                                    //if the first line item's Order id is null, all list item for the cart id has all null for order id
                                    //and it means the cart isn't proceeded yet
                                    //so in order to show the count of line item list beside the Cart
                                    if (lineItemListInCart[0].OrderId == null)
                                    {
                                        TempData["lineItemCount"] = lineItemListInCart.Count;
                                    }
                                    else
                                    {
                                        TempData["lineItemCount"] = 0;
                                    }
                                }
                                else
                                {
                                    TempData["lineItemCount"] = 0;
                                }

                                FormsAuthentication.SetAuthCookie(user.UserName, false);

                                return RedirectToAction("Index", "Game");
                            }
                        }
                        else
                        {
                            //increase the attempt number until 3
                            user.Attempts++;
                            db.Account.Add(user);
                            db.Entry(user).State = EntityState.Modified;
                            db.SaveChanges();
                            TempData["attempts"] = user.Attempts;

                            if (user.Attempts == 3)
                            {
                                ModelState.AddModelError("Password", "Your Account is locked out by 3 times wrong password input");
                            }
                            else
                            {
                                var left = 3 - user.Attempts;
                                ModelState.AddModelError("Password", "Wrong password - " + left + " Left");
                            }
                            return View("Login");
                        }
                    }

                }
            }
            // If we got this far, something failed, redisplay form
            //TempData["message"] = "Something wrong";
            return View("Login");

        }

        // POST: Account/Signup
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Signup(Account account, Person person)
        {
            CaptchaResponse response = ValidateCaptcha(Request["g-recaptcha-response"]);
            if (response.Success && ModelState.IsValid)
            {
                var user = db.Account.SingleOrDefault(x => x.UserName == account.UserName);
                if (user != null)
                {
                    ModelState.AddModelError("UserName", "Duplicated User Name");
                    //TempData["message"] = "Duplicated User Name";
                    return View("Signup");
                }
                else
                {
                    TempData["message"] = "Successful Signup";

                    Customer customer = new Customer
                    {
                        PersonId = person.PersonId
                    };
                    Address addressBilling = new Address
                    {
                        PersonId = person.PersonId,
                        AddressType = AddressType.Billing
                    };
                    Address addressShipping = new Address
                    {
                        PersonId = person.PersonId,
                        AddressType = AddressType.Shipping
                    };
                    person.Addresses.Add(addressBilling);
                    person.Addresses.Add(addressShipping);
                    UserRole userRole = new UserRole
                    {
                        AccountId = account.AccountId,
                        RoleId = 1
                    };
                    db.Person.Add(person);
                    db.Account.Add(account);
                    db.Customer.Add(customer);
                    db.Address.Add(addressBilling);
                    db.Address.Add(addressShipping);
                    db.UserRole.Add(userRole);
                    db.SaveChanges();

                    var ActivationURL = $"/Account/ActivateAccount?userName={account.UserName}&email={account.Person.Email}";
                    // generate the URI with http://localhost
                    var URILink = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, ActivationURL);
                    // Email subject and body
                    var subject = "Registration Confirmation Request";
                    var bodyHtml = $"Hi @{account.UserName}, <br/><br/> Thanks for signing up to our Conestoga GameStore account. Please <a href='{URILink}'>click here</a> to activate your account.<br/><br/>Thank you! ";

                    SendEmail(account.Person.Email, bodyHtml, subject);

                    TempData["message"] = "Confirmation link to activate your account has been sent through email. Do check your spam folder.";

                    return RedirectToAction("Login");
                }
            }
            else
            {
                TempData["message"] = $"Google ReCaptcha Error: {response.ErrorMessage}";
                return View("Signup");
            }
        }

        /// Validate Captcha  
        public static CaptchaResponse ValidateCaptcha(string response)
        {
            string secret = System.Web.Configuration.WebConfigurationManager.AppSettings["recaptchaPrivateKey"];
            var client = new WebClient();
            var jsonResult = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));
            return JsonConvert.DeserializeObject<CaptchaResponse>(jsonResult.ToString());
        }

        public ActionResult ActivateAccount(string userName, string email)
        {
            if (ModelState.IsValid)
            {
                var account = db.Account.Where(a => a.UserName == userName && a.Person.Email == email).FirstOrDefault();
                if(account == null)
                {
                    TempData["message"] = $"Account with user name {userName} doesn't exist.";
                }
                else
                {
                    account.AccountState = AccountState.Active;
                    db.Entry(account).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["message"] = $"Your Account has been successfully activated..";
                }
            }
            return RedirectToAction("Login");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Clear();

            TempData["message"] = null;
            TempData["logout"] = "logout";

            return RedirectToAction("Index","Game");
        }

        // POST: Account/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AccountId,AccountState,UserName,Password,ReceivePromotionalEmail,CreatedOn,PersonId")] Account account)
        {
            if (ModelState.IsValid)
            {
                db.Account.Add(account);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PersonId = new SelectList(db.Person, "PersonId", "FirstName", account.PersonId);
            return View(account);
        }

        // GET: Account/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Account account = db.Account.Find(id);
            if (account == null)
            {
                return HttpNotFound();
            }
            ViewBag.PersonId = new SelectList(db.Person, "PersonId", "FirstName", account.PersonId);
            return View(account);
        }

        // POST: Account/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AccountId,AccountState,UserName,Password,ReceivePromotionalEmail,CreatedOn,PersonId")] Account account)
        {
            if (ModelState.IsValid)
            {
                db.Entry(account).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PersonId = new SelectList(db.Person, "PersonId", "FirstName", account.PersonId);
            return View(account);
        }


        // GET: Account/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Account account = db.Account.Find(id);
            if (account == null)
            {
                return HttpNotFound();
            }
            return View(account);
        }

        // POST: Account/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Account account = db.Account.Find(id);
            db.Account.Remove(account);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult ForgotPassword()
        {
            return View("ForgotPassword");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(string userName)
        {
            if (ModelState.IsValid)
            {
                var account = db.Account.Where(a => a.UserName == userName).FirstOrDefault();
                if (account != null)
                {
                    // generate a new GUID on every click
                    string resetToken = Guid.NewGuid().ToString();
                    // reset url string
                    var resetURL = "/Account/ResetPassword?resetCode=" + resetToken;
                    // generate the URI with http://localhost
                    var URILink = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, resetURL);

                    // save password reset token to db
                    account.PasswordResetCode = resetToken;
                    db.SaveChanges();

                    // Email subject and body
                    var subject = "Password Reset Request";
                    var bodyHtml = $"Hi @{account.UserName}, <br/> We received a password reset request for your Conestoga Virtual GameStore Account. Click the link below to reset it. <br/><br/><a href='{URILink}'>{URILink}<a/><br/><br/>Please ignore this email if you did not request a password reset.";

                    // MISSING - VALIDATION IS EMAIL IS NULL

                    SendEmail(account.Person.Email, bodyHtml, subject);

                    TempData["message"] = "Reset password link has been emailed to you.";
                }
            }
            return View();
        }

        private void SendEmail(string EmailTo, string bodyHtml, string subject)
        {

            // fetch app settings from the web.config file
            var EmailFrom = ConfigurationManager.AppSettings.Get("EmailFrom");
            var EmailPassword = ConfigurationManager.AppSettings.Get("EmailPassword");
            var SMTPHost = ConfigurationManager.AppSettings.Get("SMTPHost");
            var SMTPPort = Convert.ToInt32(ConfigurationManager.AppSettings.Get("SMTPPort"));

            // MailMessage(From, To)
            MailMessage mail = new MailMessage(EmailFrom, EmailTo)
            {
                Subject = subject,
                Body = bodyHtml,

                IsBodyHtml = true
            };
            SmtpClient smtp = new SmtpClient
            {
                Host = SMTPHost,
                EnableSsl = true
            };

            NetworkCredential NetworkCred = new NetworkCredential(EmailFrom, EmailPassword);
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = SMTPPort;
            smtp.Send(mail);
        }

        public ActionResult ResetPassword(string resetCode)
        {
            if(string.IsNullOrWhiteSpace(resetCode))
            {
                TempData["message"] = "Reset Code is not valid. Please try again.";
                return RedirectToAction("Login");
            }

            // verify if code is linked to any account in database
            var account = db.Account.Where(a => a.PasswordResetCode == resetCode).FirstOrDefault();
            if(account == null)
            {
                TempData["message"] = "There is no account with this username.";
            }
            else
            {
                ResetPasswordModel resetPassword = new ResetPasswordModel
                {
                    ResetCode = resetCode
                };
                return View("ResetPassword");
            }
            return RedirectToAction("Login");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordModel resetPasswordModel)
        {
            if (ModelState.IsValid)
            {
                var account = db.Account.Where(a => a.PasswordResetCode == resetPasswordModel.ResetCode).FirstOrDefault();
                if (account != null)
                {
                    account.Password = resetPasswordModel.NewPassword;
                    account.PasswordResetCode = "";
                    account.Attempts = 0;
                    db.SaveChanges();
                    TempData["message"] = "New password updated successfully";
                    return RedirectToAction("Login");
                }
            }
            else
            {
                TempData["message"] = "There is an error processing your request. Please try again!";
            }
            return View("ResetPassword");
        }
    }
}
