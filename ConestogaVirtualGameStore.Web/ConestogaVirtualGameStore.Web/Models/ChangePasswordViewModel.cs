﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ConestogaVirtualGameStore.Web.Models
{
    public class ChangePasswordViewModel
    {
        public string OldPassword { get; set; }
        [RegularExpression(@"^.*(?=.{6,18})(?=.*\d)(?=.*[A-Za-z])(?=.*[@%&#]{0,}).*$", ErrorMessage = "Password doesn't meet the requirements")]
        public string NewPassword { get; set; }

        public string ConfirmPassword { get; set; }
    }
}