﻿using ConestogaVirtualGameStore.Framework.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConestogaVirtualGameStore.Web.Models
{
    public class ProfileViewModel
    {
        public Person Person {get; set;}

        public Account Account { get; set; }

        public Address BillingAddress { get; set; }

        public Address ShippingAddress { get; set; }

        public bool UseSameAddress { get; set; }

        public void CopyAddresses()
        {
            ShippingAddress.City = BillingAddress.City;
            ShippingAddress.Country = BillingAddress.Country;
            ShippingAddress.PostalCode = BillingAddress.PostalCode;
            ShippingAddress.Province = BillingAddress.Province;
            ShippingAddress.Street = BillingAddress.Street;
        }
    }
}