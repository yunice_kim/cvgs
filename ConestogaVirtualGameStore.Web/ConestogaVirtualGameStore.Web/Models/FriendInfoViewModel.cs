﻿using ConestogaVirtualGameStore.Framework.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConestogaVirtualGameStore.Web.Models
{
    public class FriendInfoViewModel
    {
        public Friend Friend { get; set; }
        public Account Account { get; set; }
    }
}